﻿namespace Neusta.Sitzplan.Api.Tests.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Moq;
    using Neusta.Sitzplan.Api.Controllers;
    using Neusta.Sitzplan.Converters;
    using Neusta.Sitzplan.Services;
    using NUnit.Framework;

    [TestFixture]
    public class ImportControllerTests
    {
        private ImportController importController;
        private Mock<ICsvToStringConverter> csvToStringConverterMock;
        private Mock<IImportService> importServiceMock;
        private Mock<ILogger<ImportController>> mockedLogger;

        [SetUp]
        public void SetUp()
        {
            this.importServiceMock = new Mock<IImportService>();
            this.mockedLogger = new Mock<ILogger<ImportController>>();
            this.csvToStringConverterMock = new Mock<ICsvToStringConverter>();
            this.importController = new ImportController(
                this.importServiceMock.Object,
                this.mockedLogger.Object,
                this.csvToStringConverterMock.Object);
        }

        [Test]
        public async Task ImportRoomsAsyncCallsGetContentFromFileAsync()
        {
            // Arrange
            Mock<IFormFile> importFile = new Mock<IFormFile>();

            // Act
            await this.importController.ImportRoomsAsync(importFile.Object).ConfigureAwait(false);

            // Assert
            this.csvToStringConverterMock.Verify(x => x.GetContentFromFileAsync(importFile.Object));
        }

        [Test]
        public async Task ImportRoomsAsyncCallsImportRooms()
        {
            // Arrange
            Mock<IFormFile> importFile = new Mock<IFormFile>();
            List<string> rooms = new List<string>();
            this.csvToStringConverterMock.Setup(x => x.GetContentFromFileAsync(importFile.Object))
                .Returns(Task.FromResult(rooms));

            // Act
            await this.importController.ImportRoomsAsync(importFile.Object).ConfigureAwait(false);

            // Assert
            this.importServiceMock.Verify(x => x.ImportRooms(rooms));
        }

        [Test]
        public async Task ImportRoomsAsyncReturnsOkResult()
        {
            // Arrange
            Mock<IFormFile> importFile = new Mock<IFormFile>();

            // Act
            IActionResult result =
                await this.importController.ImportRoomsAsync(importFile.Object).ConfigureAwait(false);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.TypeOf<OkResult>());
        }
    }
}
