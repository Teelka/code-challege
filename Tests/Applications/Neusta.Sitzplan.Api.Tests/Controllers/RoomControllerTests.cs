﻿namespace Neusta.Sitzplan.Api.Tests.Controllers
{
    using System.Collections.Generic;
    using Moq;
    using Neusta.Sitzplan.Api.Controllers;
    using Neusta.Sitzplan.DTOs;
    using Neusta.Sitzplan.Services;
    using NUnit.Framework;

    [TestFixture]
    public class RoomControllerTests
    {
        private RoomController roomController;
        private Mock<IRoomService> mockedRoomService;

        [SetUp]
        public void SetUp()
        {
            this.mockedRoomService = new Mock<IRoomService>();
            this.roomController = new RoomController(this.mockedRoomService.Object);
        }

        [Test]
        public void GetCallsGetAllRooms()
        {
            // Arrange

            // Act
            this.roomController.Get();

            // Assert
            this.mockedRoomService.Verify(x => x.GetAllRooms());
        }

        [Test]
        public void GetCallsGetRoom()
        {
            // Arrange
            string number = "absc";

            // Act
            this.roomController.Get(number);

            // Assert
            this.mockedRoomService.Verify(x => x.GetRoom(number));
        }

        [Test]
        public void GetReturnOneRoom()
        {
            // Arrange
            RoomDto rooms = new RoomDto();
            string number = "absc";
            this.mockedRoomService.Setup(x => x.GetRoom(number)).Returns(rooms);

            // Act
            RoomDto result = this.roomController.Get(number);

            // Assert
            Assert.That(result, Is.SameAs(rooms));
        }

        [Test]
        public void GetReturnsAllRooms()
        {
            // Arrange
            List<RoomDto> roomDtos = new List<RoomDto>();
            this.mockedRoomService.Setup(x => x.GetAllRooms()).Returns(roomDtos);

            // Act
            List<RoomDto> result = this.roomController.Get();

            // Assert
            Assert.That(result, Is.SameAs(roomDtos));
        }
    }
}
