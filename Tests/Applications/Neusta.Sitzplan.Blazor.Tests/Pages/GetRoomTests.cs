﻿namespace Neusta.Sitzplan.Blazor.Tests.Pages
{
    using System.Collections.Generic;
    using System.Text.Json;
    using System.Threading.Tasks;
    using Bunit;
    using Microsoft.Extensions.DependencyInjection;
    using Moq;
    using Neusta.Sitzplan.Api.Models;
    using Neusta.Sitzplan.Blazor.Pages;
    using Neusta.Sitzplan.Blazor.Services;
    using Neusta.Sitzplan.Blazor.SitzplanApiService;
    using NUnit.Framework;
    using TestContext = Bunit.TestContext;

    [TestFixture]
    public class GetRoomTests : TestContext
    {
        private Mock<IRoomService> roomService;

        [OneTimeSetUp]
        public void SetUp()
        {
            this.roomService = new Mock<IRoomService>();
            this.Services.AddSingleton(this.roomService.Object);
        }

        [Test]
        public void GetRoomWithButtonClickedAndNoRoomNumberCallsGetRooms()
        {
            // Arrange
            string roomNumber = null;
            IRenderedComponent<GetRoom> component = this.RenderComponent<GetRoom>();

            // Act
            component.Find("button").Click();


            //Assert
            this.roomService.Verify(x => x.GetRoom(roomNumber));
        }

        [Test]
        public void GetRoomWithButtonClickedAndRoomNumberCallsGetRooms()
        {
            // Arrange
            IRenderedComponent<GetRoom> component = this.RenderComponent<GetRoom>();
            component.Find("input").Input("3");

            // Act
            component.Find("button").Click();


            //Assert
            this.roomService.Verify(x => x.GetRoom("3"));
        }

        [Test]
        public void GetRoomWithNoButtonClickedShowsInitialHtml()
        {
            // Arrange

            // Act
            IRenderedComponent<GetRoom> cut = this.RenderComponent<GetRoom>();

            // Assert 
            string initialExpectedHtml =
                @"<h1>Raumdaten</h1>
                  <p>Hier sind die Räume und die Nutzer/innen des Raumes zu finden.</p>
                  <input >
                  <button class='btn btn-primary' >Finde Raum</button>
                  <p>Suche einen Raum indem du die Raumnummer eintippst.</p>";
            cut.MarkupMatches(initialExpectedHtml);
        }

        [Test]
        public void GetRoomWithButtonClickedAndNoRoomFoundShowsHtml()
        {
            // Arrange
            IRenderedComponent<GetRoom> component = this.RenderComponent<GetRoom>();
            ErrorResponse response = new ErrorResponse { Message = "test", Code = 5 };
            this.roomService.Setup(x => x.GetRoom(null))
                .Throws(new ApiException("test", 400, JsonSerializer.Serialize(response), null, null));

            // Act
            component.Find("button").Click();

            //Assert
            string initialExpectedHtml =
                @"<h1>Raumdaten</h1>
                  <p>Hier sind die Räume und die Nutzer/innen des Raumes zu finden.</p>
                  <input >
                  <button class='btn btn-primary' >Finde Raum</button>
                  <p>Es gibt keinen Raum mit der genannten Raumnummer.</p>";
            component.MarkupMatches(initialExpectedHtml);
        }

        [Test]
        public void GetRoomWithButtonClickedAndNoRoomFoundAndUnknownErrorShowsHtml()
        {
            // Arrange
            IRenderedComponent<GetRoom> component = this.RenderComponent<GetRoom>();
            ErrorResponse response = new ErrorResponse { Message = "test", Code = 3 };
            this.roomService.Setup(x => x.GetRoom(null))
                .Throws(new ApiException("test", 400, JsonSerializer.Serialize(response), null, null));

            // Act
            component.Find("button").Click();

            //Assert
            string initialExpectedHtml =
                @"<h1>Raumdaten</h1>
                  <p>Hier sind die Räume und die Nutzer/innen des Raumes zu finden.</p>
                  <input >
                  <button class='btn btn-primary' >Finde Raum</button>
                  <p>Ein unbekannter Fehler ist passiert.</p>";
            component.MarkupMatches(initialExpectedHtml);
        }

        [Test]
        public void GetRoomWithButtonClickedAndNoRoomFoundAndJsonExceptionThrownShowsHtml()
        {
            // Arrange
            IRenderedComponent<GetRoom> component = this.RenderComponent<GetRoom>();
            this.roomService.Setup(x => x.GetRoom(null))
                .Throws(new ApiException("test", 400, "", null, null));

            // Act
            component.Find("button").Click();

            //Assert
            string initialExpectedHtml =
                @"<h1>Raumdaten</h1>
                  <p>Hier sind die Räume und die Nutzer/innen des Raumes zu finden.</p>
                  <input >
                  <button class='btn btn-primary' >Finde Raum</button>
                  <p>Ein unbekannter Fehler ist passiert.</p>";
            component.MarkupMatches(initialExpectedHtml);
        }

        [Test]
        public void GetRoomsWithButtonClickedAndNoRoomNumberShowsHtmlWithTable()
        {
            // Arrange
            PersonDto person = new PersonDto
            {
                First_name = "Nadine",
                Title = "Dr.",
                Last_name = "Meyer",
                Name_addition = "van",
                Ldapuser = "Nmeyer",
            };
            RoomDto room = new RoomDto { Room = "1111", People = new List<PersonDto> { person } };
            this.roomService.Setup(x => x.GetRoom(null)).Returns(Task.FromResult(room));
            IRenderedComponent<GetRoom> component = this.RenderComponent<GetRoom>();

            // Act
            component.Find("button").Click();

            // Assert 
            string expectedHtml =
                @"<h1>Raumdaten</h1>
                <p>Hier sind die Räume und die Nutzer/innen des Raumes zu finden.</p>
                <input >
                <button class='btn btn-primary' >Finde Raum</button>
                <table class='table'>
                <thead>
                <tr>
                <th>Raumnummer</th>
                <th>Nutzer/innen des Raumes
                </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                <td>1111</td>
                </tr>
                <tr>
                <td>Dr. Nadine van Meyer Nmeyer</td>
                </tr>
                </tbody>
                </table>";
            component.MarkupMatches(expectedHtml);
        }
    }
}
