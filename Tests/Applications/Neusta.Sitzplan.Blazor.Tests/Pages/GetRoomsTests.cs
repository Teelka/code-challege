﻿namespace Neusta.Sitzplan.Blazor.Tests.Pages
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Bunit;
    using Microsoft.Extensions.DependencyInjection;
    using Moq;
    using Neusta.Sitzplan.Blazor.Pages;
    using Neusta.Sitzplan.Blazor.Services;
    using Neusta.Sitzplan.Blazor.SitzplanApiService;
    using NUnit.Framework;
    using TestContext = Bunit.TestContext;

    [TestFixture]
    public class GetRoomsTests : TestContext
    {
        private Mock<IRoomService> roomService;

        [OneTimeSetUp]
        public void SetUp()
        {
            this.roomService = new Mock<IRoomService>();
            this.Services.AddSingleton(this.roomService.Object);
        }

        [Test]
        public void GetRoomsCallsGetRooms()
        {
            // Arrange

            // Act
            this.RenderComponent<GetRooms>();

            //Assert
            this.roomService.Verify(x => x.GetRooms());
        }

        [Test]
        public void GetRoomsWithNoRoomsShowInitialHtml()
        {
            // Arrange
            this.roomService.Setup(x => x.GetRooms()).Returns(Task.FromResult<List<RoomDto>>(null));

            // Act
            IRenderedComponent<GetRooms> cut = this.RenderComponent<GetRooms>();

            // Assert 
            string initialExpectedHtml =
                @"<h1>Raumdaten</h1>
                        <p>Hier sind die Räume und die Nutzer/innen des Raumes zu finden.</p>
                        <p><em>Loading...</em></p>";
            cut.MarkupMatches(initialExpectedHtml);
        }

        [Test]
        public void GetRoomsWithOneRoomShowsHtmlWithTable()
        {
            // Arrange
            PersonDto person = new PersonDto
            {
                First_name = "Nadine",
                Title = "Dr.",
                Last_name = "Meyer",
                Name_addition = "van",
                Ldapuser = "Nmeyer",
            };
            RoomDto room = new RoomDto { Room = "1111", People = new List<PersonDto> { person } };
            List<RoomDto> rooms = new List<RoomDto> { room };
            this.roomService.Setup(x => x.GetRooms()).Returns(Task.FromResult(rooms));

            // Act
            IRenderedComponent<GetRooms> cut = this.RenderComponent<GetRooms>();

            // Assert 
            string expectedHtml =
                @"<h1>Raumdaten</h1>
                        <p>Hier sind die Räume und die Nutzer/innen des Raumes zu finden.</p>
                        <table class=table>
                <thead>
                <tr>
                <th>Raumnummer</th>
                <th>Nutzer/innen des Raumes
                </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                <td>1111</td>
                </tr>
                <tr>
                <td>Dr. Nadine van Meyer Nmeyer</td>
                </tr>
                </tbody>
                </table>";
            cut.MarkupMatches(expectedHtml);
        }
    }
}
