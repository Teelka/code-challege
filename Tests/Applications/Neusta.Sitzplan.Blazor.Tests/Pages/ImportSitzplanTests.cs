﻿namespace Neusta.Sitzplan.Blazor.Tests.Pages
{
    using Bunit;
    using Bunit.TestDoubles.JSInterop;
    using Microsoft.Extensions.DependencyInjection;
    using Moq;
    using Neusta.Sitzplan.Blazor.Pages;
    using Neusta.Sitzplan.Blazor.Services;
    using NUnit.Framework;
    using TestContext = Bunit.TestContext;

    [TestFixture]
    public class ImportSitzplanTests : TestContext
    {
        private Mock<IImportService> importServiceMock;

        [OneTimeSetUp]
        public void SetUp()
        {
            this.importServiceMock = new Mock<IImportService>();
            this.Services.AddSingleton(this.importServiceMock.Object);
            this.Services.AddMockJSRuntime();
        }

        [Test]
        public void ImportShowsInitialHtml()
        {
            // Arrange

            // Act
            IRenderedComponent<Import> component = this.RenderComponent<Import>();

            //Assert
            string initialExpectedHtml =
                @"<h1>Importiere Sitzplan Daten</h1>
                <input type='file' >
                <button class='btn btn-primary' >Importiere Sitzplan</button>
                <p></p>";
            component.MarkupMatches(initialExpectedHtml);
        }
    }
}
