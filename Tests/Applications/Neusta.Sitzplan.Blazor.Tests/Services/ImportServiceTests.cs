﻿namespace Neusta.Sitzplan.Blazor.Tests.Services
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using BlazorInputFile;
    using Moq;
    using Moq.Protected;
    using Neusta.Sitzplan.Blazor.Services;
    using Neusta.Sitzplan.Blazor.SitzplanApiService;
    using NUnit.Framework;

    public class ImportServiceTests
    {
        [Test]
        public async Task ImportFileCallsSendAsync()
        {
            // ARRANGE
            IFileListEntry importFile = SetUpIFileListEntryMock();
            const string address = "http://test.com/";
            Mock<HttpMessageHandler> handlerMock = createHttpMessageHandlerMock();
            SitzplanApi sitzplanApi = CreateSitzplanApi(handlerMock, address);
            ImportService importService = new ImportService(sitzplanApi);

            // Act
            await importService.ImportFile(importFile);

            //Assert
            Uri expectedUri = new Uri(address + "api/Import");
            handlerMock.Protected()
                .Verify(
                    "SendAsync",
                    Times.Exactly(1),
                    ItExpr.Is<HttpRequestMessage>(
                        req =>
                            req.Method == HttpMethod.Post &&
                            req.RequestUri == expectedUri
                    ),
                    ItExpr.IsAny<CancellationToken>()
                );
        }

        private static IFileListEntry SetUpIFileListEntryMock()
        {
            Mock<IFileListEntry> file = new Mock<IFileListEntry>();
            IFileListEntry importFile = file.Object;
            string test = "Testing";
            byte[] byteArray = Encoding.ASCII.GetBytes(test);
            MemoryStream stream = new MemoryStream(byteArray);
            file.Setup(s => s.Data).Returns(stream);
            file.Setup(s => s.Name).Returns("test");
            return importFile;
        }

        private static Mock<HttpMessageHandler> createHttpMessageHandlerMock()
        {
            return new Mock<HttpMessageHandler>(MockBehavior.Strict);
        }

        private static SitzplanApi CreateSitzplanApi(Mock<HttpMessageHandler> handlerMock,
            string address)
        {
            handlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(
                    new HttpResponseMessage { StatusCode = HttpStatusCode.OK, Content = new StringContent("test") })
                .Verifiable();
            HttpClient httpClient = new HttpClient(handlerMock.Object) { BaseAddress = new Uri(address) };
            SitzplanApi sitzplanApi = new SitzplanApi(address, httpClient);

            return sitzplanApi;
        }
    }
}
