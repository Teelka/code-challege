﻿namespace Neusta.Sitzplan.Blazor.Tests.Services
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Text.Json;
    using System.Threading;
    using System.Threading.Tasks;
    using Moq;
    using Moq.Protected;
    using Neusta.Sitzplan.Blazor.Services;
    using Neusta.Sitzplan.Blazor.SitzplanApiService;
    using NUnit.Framework;

    public class RoomServiceTests
    {
        [Test]
        public async Task GetRoomsReturnListOfRooms()
        {
            // ARRANGE
            const string roomNumber = "1111";
            List<RoomDto> rooms = CreateRooms(roomNumber);
            const string address = "http://test.com/";
            Mock<HttpMessageHandler> handlerMock = createHttpMessageHandlerMock();
            SitzplanApi sitzplanApi = CreateSitzplanApi(rooms, handlerMock, address);

            RoomService roomService = new RoomService(sitzplanApi);

            // Act
            List<RoomDto> result = await roomService.GetRooms();

            //Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(1));
            Assert.That(result[0].Room, Is.EqualTo(roomNumber));
        }

        [Test]
        public async Task GetRoomsCallsSendAsync()
        {
            // ARRANGE
            const string roomNumber = "1111";
            List<RoomDto> rooms = CreateRooms(roomNumber);
            const string address = "http://test.com/";
            Mock<HttpMessageHandler> handlerMock = createHttpMessageHandlerMock();
            SitzplanApi sitzplanApi = CreateSitzplanApi(rooms, handlerMock, address);

            RoomService roomService = new RoomService(sitzplanApi);
            // Act
            await roomService.GetRooms();

            //Assert
            Uri expectedUri = new Uri(address + "api/Room");
            handlerMock.Protected()
                .Verify(
                    "SendAsync",
                    Times.Exactly(1),
                    ItExpr.Is<HttpRequestMessage>(
                        req =>
                            req.Method == HttpMethod.Get &&
                            req.RequestUri == expectedUri
                    ),
                    ItExpr.IsAny<CancellationToken>()
                );
        }

        [Test]
        public async Task GetRoomReturnRoom()
        {
            // ARRANGE
            const string roomNumber = "1111";
            RoomDto room = new RoomDto { Room = roomNumber };
            const string address = "http://test.com/";
            Mock<HttpMessageHandler> handlerMock = createHttpMessageHandlerMock();
            SitzplanApi sitzplanApi = CreateSitzplanApi(room, handlerMock, address);

            RoomService roomService = new RoomService(sitzplanApi);

            // Act
            RoomDto result = await roomService.GetRoom(roomNumber);

            //Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Room, Is.EqualTo(roomNumber));
        }

        [Test]
        public async Task GetRoomCallsSendAsync()
        {
            // ARRANGE
            const string roomNumber = "1111";
            RoomDto room = new RoomDto { Room = roomNumber };
            const string address = "http://test.com/";
            Mock<HttpMessageHandler> handlerMock = createHttpMessageHandlerMock();
            SitzplanApi sitzplanApi = CreateSitzplanApi(room, handlerMock, address);
            RoomService roomService = new RoomService(sitzplanApi);

            // Act
            await roomService.GetRoom(roomNumber);

            //Assert
            Uri expectedUri = new Uri(address + "api/Room/" + roomNumber);
            handlerMock.Protected()
                .Verify(
                    "SendAsync",
                    Times.Exactly(1),
                    ItExpr.Is<HttpRequestMessage>(
                        req =>
                            req.Method == HttpMethod.Get &&
                            req.RequestUri == expectedUri
                    ),
                    ItExpr.IsAny<CancellationToken>()
                );
        }

        private static Mock<HttpMessageHandler> createHttpMessageHandlerMock()
        {
            return new Mock<HttpMessageHandler>(MockBehavior.Strict);
        }

        private static List<RoomDto> CreateRooms(string roomNumber)
        {
            RoomDto room = new RoomDto { Room = roomNumber };
            List<RoomDto> rooms = new List<RoomDto> { room };
            return rooms;
        }

        private static SitzplanApi CreateSitzplanApi(List<RoomDto> rooms,
            Mock<HttpMessageHandler> handlerMock,
            string address)
        {
            string roomsAsString = JsonSerializer.Serialize(rooms);
            handlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(
                    new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.OK, Content = new StringContent(roomsAsString),
                    })
                .Verifiable();
            HttpClient httpClient = new HttpClient(handlerMock.Object) { BaseAddress = new Uri(address) };
            SitzplanApi sitzplanApi = new SitzplanApi(address, httpClient);

            return sitzplanApi;
        }

        private static SitzplanApi CreateSitzplanApi(RoomDto room,
            Mock<HttpMessageHandler> handlerMock,
            string address)
        {
            string roomsAsString = JsonSerializer.Serialize(room);
            handlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                .ReturnsAsync(
                    new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.OK, Content = new StringContent(roomsAsString),
                    })
                .Verifiable();
            HttpClient httpClient = new HttpClient(handlerMock.Object) { BaseAddress = new Uri(address) };
            SitzplanApi sitzplanApi = new SitzplanApi(address, httpClient);

            return sitzplanApi;
        }
    }
}
