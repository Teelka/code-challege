﻿namespace Neusta.Sitzplan.Tests.Converters
{
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Moq;
    using Neusta.Sitzplan.Converters;
    using Neusta.Sitzplan.Validators;
    using NUnit.Framework;

    [TestFixture]
    public class CsvToStringConverterTests
    {
        private CsvToStringConverter csvToStringConverter;
        private Mock<IFileValidator> mockedFileValidator;

        [SetUp]
        public void SetUp()
        {
            this.mockedFileValidator = new Mock<IFileValidator>();
            this.csvToStringConverter = new CsvToStringConverter(this.mockedFileValidator.Object);
        }

        [Test]
        public async Task GetContentFromFileAsyncCallsValidateFile()
        {
            // Arrange
            IFormFile inputFile = new FormFile(new MemoryStream(new byte[] { }), 0, 0, null, null);

            // Act
            await this.csvToStringConverter.GetContentFromFileAsync(inputFile).ConfigureAwait(false);

            // Assert
            this.mockedFileValidator.Verify(x => x.ValidateFile(inputFile));
        }

        [Test]
        public async Task GetContentFromFileAsyncReturnsContent()
        {
            // Arrange
            byte[] content = Encoding.UTF8.GetBytes(
                "1111,Dennis Fischer (dfischer)\r\n" +
                "1110,Christina Hülsemann (chuelsemann)\r\n" +
                "1109,Stefanie Borcherding (sborcherding)");

            IFormFile inputFile = new FormFile(
                new MemoryStream(content),
                0,
                content.Length,
                "Data",
                "sitzplan.csv");

            // Act
            List<string> result =
                await this.csvToStringConverter.GetContentFromFileAsync(inputFile).ConfigureAwait(false);

            // Assert
            Assert.That(result.Count, Is.EqualTo(3));
            Assert.That(result[0], Is.EqualTo("1111,Dennis Fischer (dfischer)"));
            Assert.That(result[2], Is.EqualTo("1109,Stefanie Borcherding (sborcherding)"));
        }
    }
}
