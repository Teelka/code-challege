﻿namespace Neusta.Sitzplan.Tests.Mappers
{
    using System.Collections.Generic;
    using Neusta.Database.Entities;
    using Neusta.Sitzplan.DTOs;
    using Neusta.Sitzplan.Mappers;
    using NUnit.Framework;

    [TestFixture]
    public class RoomMapperTests
    {
        private RoomMapper roomMapper;

        [SetUp]
        public void SetUp()
        {
            this.roomMapper = new RoomMapper();
        }

        [Test]
        public void MapRoomToRoomDtoWithoutPeopleReturnsRoomDtoWithNullPeopleDto()
        {
            // Arrange
            Room room = new Room();
            room.RoomNumber = "Abcd";
            room.People = null;

            // Act
            RoomDto result = this.roomMapper.MapRoomToRoomDto(room);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.RoomNumber, Is.EqualTo(room.RoomNumber));
            Assert.That(result.People, Is.Null);
        }

        [Test]
        public void MapRoomToRoomDtoWithoutPeopleReturnsRoomDtoWithoutPeopleDto()
        {
            // Arrange
            Room room = new Room();
            room.RoomNumber = "Abcd";
            room.People = new List<Person>();

            // Act
            RoomDto result = this.roomMapper.MapRoomToRoomDto(room);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.RoomNumber, Is.EqualTo(room.RoomNumber));
            Assert.That(result.People, Is.Empty);
        }

        [Test]
        public void MapRoomToRoomDtoWithPeopleReturnsRoomDtoWithPeopleDto()
        {
            // Arrange
            Room room = new Room();
            room.RoomNumber = "Abcd";
            List<Person> people = new List<Person>();
            Person person = new Person();
            person.FirstName = "Frank";
            person.LastName = "Supper";
            person.Title = "Dr.";
            person.NameAddition = "von";
            person.Ldapuser = "fsupper";
            people.Add(person);
            room.People = people;

            // Act
            RoomDto result = this.roomMapper.MapRoomToRoomDto(room);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.RoomNumber, Is.EqualTo(room.RoomNumber));
            Assert.That(result.People, Is.Not.Null);
            Assert.That(result.People.Count, Is.EqualTo(1));
            Assert.That(result.People[0].FirstName, Is.EqualTo(room.People[0].FirstName));
            Assert.That(result.People[0].LastName, Is.EqualTo(room.People[0].LastName));
            Assert.That(result.People[0].Title, Is.EqualTo(room.People[0].Title));
            Assert.That(result.People[0].NameAddition, Is.EqualTo(room.People[0].NameAddition));
            Assert.That(result.People[0].Ldapuser, Is.EqualTo(room.People[0].Ldapuser));
        }
    }
}
