﻿namespace Neusta.Sitzplan.Tests.Parsers
{
    using Neusta.Database.Entities;
    using Neusta.Sitzplan.Exceptions;
    using Neusta.Sitzplan.Parsers;
    using NUnit.Framework;

    [TestFixture]
    public class StringToPersonParserTests
    {
        private StringToPersonParser stringToPersonParser;

        [SetUp]
        public void SetUp()
        {
            this.stringToPersonParser = new StringToPersonParser();
        }

        [TestCase("von")]
        [TestCase("van")]
        [TestCase("de")]
        public void MapStringToPersonWithDifferentNameAdditionsAndNoTitleAndNoSecondName(string nameAddition)
        {
            // Arrange
            string input = "Frank " + nameAddition + " Supper (fsupper)";

            // Act
            Person result = this.stringToPersonParser.ParseStringToPerson(input);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Title, Is.EqualTo(string.Empty));
            Assert.That(result.FirstName, Is.EqualTo("Frank"));
            Assert.That(result.NameAddition, Is.EqualTo(nameAddition));
            Assert.That(result.LastName, Is.EqualTo("Supper"));
            Assert.That(result.Ldapuser, Is.EqualTo("fsupper"));
        }

        [TestCase("von")]
        [TestCase("van")]
        [TestCase("de")]
        public void MapStringToPersonWithDifferentNameAdditionsAndTitleAndNoSecondName(string nameAddition)
        {
            // Arrange
            string input = "Dr. Frank " + nameAddition + " Supper (fsupper)";

            // Act
            Person result = this.stringToPersonParser.ParseStringToPerson(input);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Title, Is.EqualTo("Dr."));
            Assert.That(result.FirstName, Is.EqualTo("Frank"));
            Assert.That(result.NameAddition, Is.EqualTo(nameAddition));
            Assert.That(result.LastName, Is.EqualTo("Supper"));
            Assert.That(result.Ldapuser, Is.EqualTo("fsupper"));
        }

        [TestCase("von")]
        [TestCase("van")]
        [TestCase("de")]
        public void MapStringToPersonWithDifferentNameAdditionsAndTitleAndSecondName(string nameAddition)
        {
            // Arrange
            string input = "Dr. Frank Martin " + nameAddition + " Supper (fsupper)";

            // Act
            Person result = this.stringToPersonParser.ParseStringToPerson(input);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Title, Is.EqualTo("Dr."));
            Assert.That(result.FirstName, Is.EqualTo("Frank Martin"));
            Assert.That(result.NameAddition, Is.EqualTo(nameAddition));
            Assert.That(result.LastName, Is.EqualTo("Supper"));
            Assert.That(result.Ldapuser, Is.EqualTo("fsupper"));
        }

        [TestCase("von")]
        [TestCase("van")]
        [TestCase("de")]
        public void MapStringToPersonWithDifferentNameAdditionsAndNoTitleAndSecondName(string nameAddition)
        {
            // Arrange
            string input = "Frank Martin " + nameAddition + " Supper (fsupper)";

            // Act
            Person result = this.stringToPersonParser.ParseStringToPerson(input);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Title, Is.EqualTo(string.Empty));
            Assert.That(result.FirstName, Is.EqualTo("Frank Martin"));
            Assert.That(result.NameAddition, Is.EqualTo(nameAddition));
            Assert.That(result.LastName, Is.EqualTo("Supper"));
            Assert.That(result.Ldapuser, Is.EqualTo("fsupper"));
        }

        [Test]
        public void MapStringToPersonWithNoNameAdditionAndNoTitleAndNoSecondName()
        {
            // Arrange
            string input = "Frank Supper (fsupper)";

            // Act
            Person result = this.stringToPersonParser.ParseStringToPerson(input);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Title, Is.EqualTo(string.Empty));
            Assert.That(result.FirstName, Is.EqualTo("Frank"));
            Assert.That(result.NameAddition, Is.EqualTo(string.Empty));
            Assert.That(result.LastName, Is.EqualTo("Supper"));
            Assert.That(result.Ldapuser, Is.EqualTo("fsupper"));
        }

        [Test]
        public void MapStringToPersonWithNoNameAdditionAndNoTitleAndSecondName()
        {
            // Arrange
            string input = "Frank Martin Supper (fsupper)";

            // Act
            Person result = this.stringToPersonParser.ParseStringToPerson(input);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Title, Is.EqualTo(string.Empty));
            Assert.That(result.FirstName, Is.EqualTo("Frank Martin"));
            Assert.That(result.NameAddition, Is.EqualTo(string.Empty));
            Assert.That(result.LastName, Is.EqualTo("Supper"));
            Assert.That(result.Ldapuser, Is.EqualTo("fsupper"));
        }

        [Test]
        public void MapStringToPersonWithNoNameAdditionAndTitleAndNoSecondName()
        {
            // Arrange
            string input = "Dr. Frank Supper (fsupper)";

            // Act
            Person result = this.stringToPersonParser.ParseStringToPerson(input);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Title, Is.EqualTo("Dr."));
            Assert.That(result.FirstName, Is.EqualTo("Frank"));
            Assert.That(result.NameAddition, Is.EqualTo(string.Empty));
            Assert.That(result.LastName, Is.EqualTo("Supper"));
            Assert.That(result.Ldapuser, Is.EqualTo("fsupper"));
        }

        [Test]
        public void MapStringToPersonWithNoNameAdditionAndTitleAndSecondName()
        {
            // Arrange
            string input = "Dr. Frank Martin Supper (fsupper)";

            // Act
            Person result = this.stringToPersonParser.ParseStringToPerson(input);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Title, Is.EqualTo("Dr."));
            Assert.That(result.FirstName, Is.EqualTo("Frank Martin"));
            Assert.That(result.NameAddition, Is.EqualTo(string.Empty));
            Assert.That(result.LastName, Is.EqualTo("Supper"));
            Assert.That(result.Ldapuser, Is.EqualTo("fsupper"));
        }

        [Test]
        public void MapStringToPersonWithTooFewNames()
        {
            // Arrange
            string input = "Frank (fsupper)";

            // Act
            InvalidCsvDataException ex =
                Assert.Throws<InvalidCsvDataException>(() => this.stringToPersonParser.ParseStringToPerson(input));

            // Assert
            Assert.That(
                ex.Message,
                Is.EqualTo(
                    $"The people in the file should has a title (optional, value: Dr.), firstName, middleName(optional), nameAddition(optional, values: de, van, von), lastName, ldapuser (value with brackets: (ldapUserName)) : {input}"));
        }

        [TestCase("Frank Meyer noBraces")]
        [TestCase("Frank Meyer oneBrace)")]
        [TestCase("Frank Meyer (oneBrace")]
        public void MapStringToPersonWithWrongLdapuser(string input)
        {
            // Arrange

            // Act
            InvalidCsvDataException ex =
                Assert.Throws<InvalidCsvDataException>(() => this.stringToPersonParser.ParseStringToPerson(input));

            // Assert
            Assert.That(
                ex.Message,
                Is.EqualTo(
                    $"The people in the file should has a title (optional, value: Dr.), firstName, middleName(optional), nameAddition(optional, values: de, van, von), lastName, ldapuser (value with brackets: (ldapUserName)) : {input}"));
        }

        [Test]
        public void MapStringToPersonWithTooFewNamesWithTitle()
        {
            // Arrange
            string input = "Dr. Frank (fsupper)";

            // Act
            InvalidCsvDataException ex =
                Assert.Throws<InvalidCsvDataException>(() => this.stringToPersonParser.ParseStringToPerson(input));

            // Assert
            Assert.That(
                ex.Message,
                Is.EqualTo(
                    $"The people in the file should has a title (optional, value: Dr.), firstName, middleName(optional), nameAddition(optional, values: de, van, von), lastName, ldapuser (value with brackets: (ldapUserName)) : {input}"));
        }

        [Test]
        public void MapStringToPersonWithTooManyFirstNamesAndNameAdditions()
        {
            // Arrange
            string input = "Frank Stefan Sebastian van Meyer (fsupper)";

            // Act
            InvalidCsvDataException ex =
                Assert.Throws<InvalidCsvDataException>(() => this.stringToPersonParser.ParseStringToPerson(input));

            // Assert
            Assert.That(
                ex.Message,
                Is.EqualTo(
                    $"The people in the file should has a title (optional, value: Dr.), firstName, middleName(optional), nameAddition(optional, values: de, van, von), lastName, ldapuser (value with brackets: (ldapUserName)) : {input}"));
        }

        [Test]
        public void MapStringToPersonWithTooManyFirstNames()
        {
            // Arrange
            string input = "Frank Stefan Sebastian Meyer (fsupper)";

            // Act
            InvalidCsvDataException ex =
                Assert.Throws<InvalidCsvDataException>(() => this.stringToPersonParser.ParseStringToPerson(input));

            // Assert
            Assert.That(
                ex.Message,
                Is.EqualTo(
                    $"The people in the file should has a title (optional, value: Dr.), firstName, middleName(optional), nameAddition(optional, values: de, van, von), lastName, ldapuser (value with brackets: (ldapUserName)) : {input}"));
        }
    }
}
