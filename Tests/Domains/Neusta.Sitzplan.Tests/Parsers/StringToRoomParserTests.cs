﻿namespace Neusta.Sitzplan.Tests.Parsers
{
    using Moq;
    using Neusta.Database.Entities;
    using Neusta.Sitzplan.Parsers;
    using Neusta.Sitzplan.Validators;
    using NUnit.Framework;

    [TestFixture]
    public class StringToRoomParserTests
    {
        private StringToRoomParser stringToRoomParser;
        private Mock<IRoomNumberValidator> roomNumberValidatorMock;
        private Mock<IStringToPersonParser> stringToPersonParserMock;

        [SetUp]
        public void SetUp()
        {
            this.roomNumberValidatorMock = new Mock<IRoomNumberValidator>();
            this.stringToPersonParserMock = new Mock<IStringToPersonParser>();
            this.stringToRoomParser = new StringToRoomParser(
                this.roomNumberValidatorMock.Object,
                this.stringToPersonParserMock.Object);
        }

        [Test]
        public void ParseStringToRoomCallsValidateRoomNumber()
        {
            // Arrange
            string input = "1111,Frank Supper (fsupper)";

            // Act
            this.stringToRoomParser.ParseStringToRoom(input);

            // Assert
            this.roomNumberValidatorMock.Verify(x => x.ValidateRoomNumber("1111"));
        }

        [Test]
        public void ParseStringToRoomParseRoomNumber()
        {
            // Arrange
            string input = "1111,Frank Supper (fsupper)";

            // Act
            Room result = this.stringToRoomParser.ParseStringToRoom(input);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.RoomNumber, Is.EqualTo("1111"));
        }

        [Test]
        public void ParseStringToRoomWithNoPeopleReturnsNoPeople()
        {
            // Arrange
            string input = "1111,,,";

            // Act
            Room result = this.stringToRoomParser.ParseStringToRoom(input);

            // Assert
            Assert.That(result.People, Is.Null);
        }

        [Test]
        public void ParseStringToRoomWithPeopleCallsParseStringToPerson()
        {
            // Arrange
            string input = "1111,bla,blu,";

            // Act
            this.stringToRoomParser.ParseStringToRoom(input);

            // Assert
            this.stringToPersonParserMock.Verify(x => x.ParseStringToPerson("bla"));
            this.stringToPersonParserMock.Verify(x => x.ParseStringToPerson("blu"));
            this.stringToPersonParserMock.VerifyNoOtherCalls();
        }

        [Test]
        public void ParseStringToRoomWithPeopleReturnsRoomWithPeople()
        {
            // Arrange
            string input = "1111,bla,blu,";
            Person firstPerson = new Person();
            Person secondPerson = new Person();
            this.stringToPersonParserMock.Setup(x => x.ParseStringToPerson("bla")).Returns(firstPerson);
            this.stringToPersonParserMock.Setup(x => x.ParseStringToPerson("blu")).Returns(secondPerson);

            // Act
            Room result = this.stringToRoomParser.ParseStringToRoom(input);

            // Assert
            Assert.That(result.People, Is.Not.Empty);
            Assert.That(result.People.Count, Is.EqualTo(2));
            Assert.That(result.People[0], Is.SameAs(firstPerson));
            Assert.That(result.People[1], Is.SameAs(secondPerson));
        }
    }
}
