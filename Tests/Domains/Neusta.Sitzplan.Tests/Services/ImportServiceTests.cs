﻿namespace Neusta.Sitzplan.Tests.Services
{
    using System.Collections.Generic;
    using Moq;
    using Neusta.Database.Entities;
    using Neusta.Database.Repositories;
    using Neusta.Sitzplan.Parsers;
    using Neusta.Sitzplan.Services;
    using Neusta.Sitzplan.Validators;
    using NUnit.Framework;

    [TestFixture]
    public class ImportServiceTests
    {
        private ImportService importService;
        private Mock<IRoomNumberValidator> roomValidatorMock;
        private Mock<IRoomRepository> roomRepositoryMock;
        private Mock<IStringToRoomParser> stringToRoomParserMock;

        private Mock<IPersonValidator> personValidatorMock;

        [SetUp]
        public void SetUp()
        {
            this.roomValidatorMock = new Mock<IRoomNumberValidator>();
            this.personValidatorMock = new Mock<IPersonValidator>();
            this.roomRepositoryMock = new Mock<IRoomRepository>();
            this.stringToRoomParserMock = new Mock<IStringToRoomParser>();
            this.importService = new ImportService(
                this.roomValidatorMock.Object,
                this.roomRepositoryMock.Object,
                this.stringToRoomParserMock.Object,
                this.personValidatorMock.Object);
        }

        [Test]
        public void ImportRoomsCallsMapStringToRoom()
        {
            // Arrange
            string firstRoomAsString = "1111,";
            string secondRoomAsString = "2222,";
            List<string> rooms = new List<string> { firstRoomAsString, secondRoomAsString };

            // Act
            this.importService.ImportRooms(rooms);

            // Assert
            this.stringToRoomParserMock.Verify(x => x.ParseStringToRoom(firstRoomAsString));
            this.stringToRoomParserMock.Verify(x => x.ParseStringToRoom(secondRoomAsString));
            this.stringToRoomParserMock.VerifyNoOtherCalls();
        }

        [Test]
        public void ImportRoomsCallsValidateUniquenessForRoomNumber()
        {
            // Arrange

            // Act
            List<string> roomsAsString = new List<string>();
            this.importService.ImportRooms(roomsAsString);

            // Assert
            this.roomValidatorMock.Verify(x => x.ValidateUniquenessForRoomNumber(It.IsAny<List<Room>>()));
        }

        [Test]
        public void ImportRoomsCallsValidateUniquenessForPeople()
        {
            // Arrange

            // Act
            List<string> roomsAsString = new List<string>();
            this.importService.ImportRooms(roomsAsString);

            // Assert
            this.personValidatorMock.Verify(x => x.ValidateUniquenessForPeople(It.IsAny<List<Room>>()));
        }

        [Test]
        public void ImportRoomsCallsSave()
        {
            // Arrange

            // Act
            List<string> roomsAsString = new List<string>();
            this.importService.ImportRooms(roomsAsString);

            // Assert
            this.roomRepositoryMock.Verify(x => x.Save(It.IsAny<List<Room>>()));
        }
    }
}
