﻿namespace Neusta.Sitzplan.Tests.Services
{
    using System.Collections.Generic;
    using Moq;
    using Neusta.Database.Entities;
    using Neusta.Database.Repositories;
    using Neusta.Sitzplan.DTOs;
    using Neusta.Sitzplan.Exceptions;
    using Neusta.Sitzplan.Mappers;
    using Neusta.Sitzplan.Services;
    using NUnit.Framework;

    [TestFixture]
    public class RoomServiceTests
    {
        private RoomService roomService;
        private Mock<IRoomRepository> mockedRoomRepository;
        private Mock<IRoomMapper> mockedRoomMapper;

        [SetUp]
        public void SetUp()
        {
            this.mockedRoomRepository = new Mock<IRoomRepository>();
            this.mockedRoomMapper = new Mock<IRoomMapper>();
            this.roomService = new RoomService(this.mockedRoomRepository.Object, this.mockedRoomMapper.Object);
        }

        [Test]
        public void GetAllRoomsCallsGetAllRoomsOnRepository()
        {
            // Arrange
            this.mockedRoomRepository.Setup(x => x.GetAllRooms()).Returns(new List<Room>());

            // Act
            this.roomService.GetAllRooms();

            // Assert
            this.mockedRoomRepository.Verify(x => x.GetAllRooms());
        }

        [Test]
        public void GetAllRoomsCallsMapRoomToRoomDto()
        {
            // Arrange
            Room room = new Room();
            this.mockedRoomRepository.Setup(x => x.GetAllRooms()).Returns(new List<Room> { room });

            // Act
            this.roomService.GetAllRooms();

            // Assert
            this.mockedRoomMapper.Verify(x => x.MapRoomToRoomDto(room));
            this.mockedRoomMapper.VerifyNoOtherCalls();
        }

        [Test]
        public void GetAllRoomsReturnsAllRooms()
        {
            // Arrange
            Room room = new Room();
            this.mockedRoomRepository.Setup(x => x.GetAllRooms()).Returns(new List<Room> { room });
            RoomDto roomDto = new RoomDto();
            this.mockedRoomMapper.Setup(x => x.MapRoomToRoomDto(room)).Returns(roomDto);

            // Act
            List<RoomDto> result = this.roomService.GetAllRooms();

            // Assert
            Assert.That(result.Count, Is.EqualTo(1));
            Assert.That(result, Has.Member(roomDto));
        }

        [Test]
        public void GetRoomCallsFindRoomFromRepository()
        {
            // Arrange
            string roomNumber = "abcd";
            Room room = new Room();
            this.mockedRoomRepository.Setup(x => x.FindRoom(roomNumber)).Returns(room);

            // Act
            this.roomService.GetRoom(roomNumber);

            // Assert
            this.mockedRoomRepository.Verify(x => x.FindRoom(roomNumber));
        }

        [Test]
        public void GetRoomCallsMapRoomToRoomDto()
        {
            // Arrange
            string roomNumber = "abcd";
            Room room = new Room();
            this.mockedRoomRepository.Setup(x => x.FindRoom(roomNumber)).Returns(room);

            // Act
            this.roomService.GetRoom(roomNumber);

            // Assert
            this.mockedRoomMapper.Verify(x => x.MapRoomToRoomDto(room));
        }

        [Test]
        public void GetRoomReturnsCorrectRoom()
        {
            // Arrange
            string roomNumber = "abcd";
            Room room = new Room();
            this.mockedRoomRepository.Setup(x => x.FindRoom(roomNumber)).Returns(room);
            RoomDto roomDto = new RoomDto();
            this.mockedRoomMapper.Setup(x => x.MapRoomToRoomDto(room)).Returns(roomDto);

            // Act
            RoomDto result = this.roomService.GetRoom(roomNumber);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.SameAs(roomDto));
        }

        [Test]
        public void GetRoomWithNuoRoomFoundThrowsException()
        {
            // Arrange
            string roomNumber = "abcd";
            this.mockedRoomRepository.Setup(x => x.FindRoom(roomNumber)).Returns((Room)null);

            // Act
            TestDelegate testDelegate = () => this.roomService.GetRoom(roomNumber);

            // Assert
            RoomNotFoundException ex = Assert.Throws<RoomNotFoundException>(testDelegate);
            Assert.That(ex.Message, Is.EqualTo("There is no room with number abcd"));
        }
    }
}
