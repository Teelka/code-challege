﻿namespace Neusta.Sitzplan.Tests.Validators
{
    using System.IO;
    using Microsoft.AspNetCore.Http;
    using Neusta.Sitzplan.Exceptions;
    using Neusta.Sitzplan.Validators;
    using NUnit.Framework;

    [TestFixture]
    public class FileValidatorTests
    {
        private FileValidator fileValidator;

        [SetUp]
        public void SetUp()
        {
            this.fileValidator = new FileValidator();
        }

        [Test]
        public void ValidateFileWithWithNoContentThrowsException()
        {
            // Arrange
            IFormFile inputFile;
            using (FileStream stream = File.OpenRead("sitzplan.csv"))
            {
                inputFile = new FormFile(stream, 0, 0, null, Path.GetFileName(stream.Name))
                {
                    Headers = new HeaderDictionary(), ContentType = "text/csv",
                };
            }

            // Act
            InvalidCsvDataException ex =
                Assert.Throws<InvalidCsvDataException>(() => this.fileValidator.ValidateFile(inputFile));

            // Assert
            Assert.That(ex.Message, Is.EqualTo("File contains no Data."));
        }

        [Test]
        public void ValidateFileWithWithNoFileThrowsException()
        {
            // Arrange

            // Act
            InvalidCsvDataException ex =
                Assert.Throws<InvalidCsvDataException>(() => this.fileValidator.ValidateFile(null));

            // Assert
            Assert.That(ex.Message, Is.EqualTo("No file was passed as parameter."));
        }
    }
}
