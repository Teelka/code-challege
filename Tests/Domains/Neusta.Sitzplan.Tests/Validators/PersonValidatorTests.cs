﻿namespace Neusta.Sitzplan.Tests.Validators
{
    using System.Collections.Generic;
    using System.Linq;
    using Neusta.Database.Entities;
    using Neusta.Sitzplan.Exceptions;
    using Neusta.Sitzplan.Validators;
    using NUnit.Framework;

    [TestFixture]
    public class PersonValidatorTests
    {
        private PersonValidator personValidator;

        [SetUp]
        public void SetUp()
        {
            this.personValidator = new PersonValidator();
        }

        [Test]
        public void ValidateUniquenessForPeopleWithDifferenPersonInTwoRoomsThrowsNoException()
        {
            // Arrange
            Room firstRoom = new Room();
            Person personForFirstRoom = new Person { Ldapuser = "temkes" };
            List<Person> peopleForFirstRoom = new List<Person> { personForFirstRoom };
            firstRoom.People = peopleForFirstRoom;

            Room secondRoom = new Room();
            Person personForSecondRoom = new Person { Ldapuser = "dmüller" };
            List<Person> peopleForSecondRoom = new List<Person> { personForSecondRoom };
            secondRoom.People = peopleForSecondRoom;

            List<Room> rooms = new List<Room> { firstRoom, secondRoom };

            // Act
            this.personValidator.ValidateUniquenessForPeople(rooms);

            // Assert
        }

        [Test]
        public void ValidateUniquenessForPeopleWithNoPersonsThrowsNoException()
        {
            // Arrange
            Room firstRoom = new Room();
            Person personForFirstRoom = new Person { Ldapuser = "temkes" };
            List<Person> peopleForFirstRoom = new List<Person> { personForFirstRoom };
            firstRoom.People = peopleForFirstRoom;

            Room secondRoom = new Room();
            secondRoom.People = null;

            List<Room> rooms = new List<Room> { firstRoom, secondRoom };

            // Act
            this.personValidator.ValidateUniquenessForPeople(rooms);

            // Assert
        }

        [Test]
        public void ValidateUniquenessForPeopleWithPeopleAreSeveralInOneRoomThrowsException()
        {
            // Arrange
            Room firstRoom = new Room();
            Person firstPersonForFirstRoom = new Person { Ldapuser = "temkes" };
            Person secondPersonForFirstRoom = new Person { Ldapuser = "dmüller" };
            Person thirdPersonForFirstRoom = new Person { Ldapuser = "bemue" };
            List<Person> peopleForFirstRoom = new List<Person>
            {
                firstPersonForFirstRoom, secondPersonForFirstRoom, thirdPersonForFirstRoom,
            };
            firstRoom.People = peopleForFirstRoom;

            Room secondRoom = new Room();
            Person personForSecondRoom = new Person { Ldapuser = "temkes" };
            List<Person> peopleForSecondRoom = new List<Person> { personForSecondRoom };
            secondRoom.People = peopleForSecondRoom;

            Room thirdRoom = new Room();
            Person firstPersonForThirdRoom = new Person { Ldapuser = "temkes" };
            Person secondPersonForThirdRoom = new Person { Ldapuser = "bemue" };

            List<Person> peopleForThirdRoom = new List<Person> { firstPersonForThirdRoom, secondPersonForThirdRoom };
            thirdRoom.People = peopleForThirdRoom;

            List<Room> rooms = new List<Room> { firstRoom, secondRoom, thirdRoom };

            // Act
            NonUniquePersonException ex =
                Assert.Throws<NonUniquePersonException>(() => this.personValidator.ValidateUniquenessForPeople(rooms));

            // Assert
            Assert.That(
                ex.Message,
                Is.EqualTo("The file should contains only unique people.The ldapuser are not unique: temkes, bemue"));
        }

        [Test]
        public void ValidateUniquenessForPeopleWithPersonIsDifferentInOneRoomThrowsNoException()
        {
            // Arrange
            Room firstRoom = new Room();
            Person firstPerson = new Person { Ldapuser = "temkes" };
            Person secondPerson = new Person { Ldapuser = "dmüller" };
            List<Person> peopleForFirstRoom = new List<Person> { firstPerson, secondPerson };
            firstRoom.People = peopleForFirstRoom;
            List<Room> rooms = new List<Room> { firstRoom };

            // Act
            this.personValidator.ValidateUniquenessForPeople(rooms);

            // Assert
        }

        [Test]
        public void ValidateUniquenessForPeopleWithPersonIsTwiceInOneRoomThrowsException()
        {
            // Arrange
            Room firstRoom = new Room();
            Person firstPersonForFirstRoom = new Person { Ldapuser = "temkes" };
            Person secondPersonForFirstRoom = new Person { Ldapuser = "dmüller" };
            List<Person> peopleForFirstRoom = new List<Person> { firstPersonForFirstRoom, secondPersonForFirstRoom };
            firstRoom.People = peopleForFirstRoom;

            Room secondRoom = new Room();
            Person personForSecondRoom = new Person { Ldapuser = "temkes" };
            List<Person> peopleForSecondRoom = new List<Person> { personForSecondRoom };
            secondRoom.People = peopleForSecondRoom;

            List<Room> rooms = new List<Room> { firstRoom, secondRoom };

            // Act
            NonUniquePersonException ex =
                Assert.Throws<NonUniquePersonException>(() => this.personValidator.ValidateUniquenessForPeople(rooms));

            // Assert
            Assert.That(
                ex.Message,
                Is.EqualTo("The file should contains only unique people.The ldapuser are not unique: temkes"));
        }

        [Test]
        public void ValidateUniquenessForPeopleWithPersonIsTwiceInTwoRoomsThrowsException()
        {
            // Arrange
            Room firstRoom = this.CreateRoomWithPeople("temkes");
            Room secondRoom = this.CreateRoomWithPeople("temkes");
            List<Room> rooms = new List<Room> { firstRoom, secondRoom };

            // Act
            NonUniquePersonException ex =
                Assert.Throws<NonUniquePersonException>(() => this.personValidator.ValidateUniquenessForPeople(rooms));

            // Assert
            Assert.That(
                ex.Message,
                Is.EqualTo("The file should contains only unique people.The ldapuser are not unique: temkes"));
        }

        private Room CreateRoomWithPeople(params string[] ldapusers)
        {
            Room room = new Room();
            room.People = ldapusers.Select(ldapuser => new Person { Ldapuser = ldapuser }).ToList();
            return room;
        }
    }
}
