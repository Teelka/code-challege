﻿namespace Neusta.Sitzplan.Tests.Validators
{
    using System.Collections.Generic;
    using Neusta.Database.Entities;
    using Neusta.Sitzplan.Exceptions;
    using Neusta.Sitzplan.Validators;
    using NUnit.Framework;

    [TestFixture]
    public class RoomNumberValidatorTests
    {
        private RoomNumberValidator roomNumberValidator;

        [SetUp]
        public void SetUp()
        {
            this.roomNumberValidator = new RoomNumberValidator();
        }

        [Test]
        public void ValidateRoomNumberWithWrongRoomNumberThrowsExceptionWithErrorCodeNonFourDigitRoomNumber()
        {
            // Arrange
            string roomNumber = "22222";

            // Act
            NonFourDigitRoomNumberException ex =
                Assert.Throws<NonFourDigitRoomNumberException>(
                    () => this.roomNumberValidator.ValidateRoomNumber(roomNumber));

            // Assert
            Assert.That(ex.Message, Is.EqualTo("This file contains a room with a non 4-digit room number: 22222"));
        }

        [Test]
        public void ValidateRoomNumberWithWrongRoomNumberThrowsNoException()
        {
            // Arrange
            string roomNumber = "2222";

            // Act
            this.roomNumberValidator.ValidateRoomNumber(roomNumber);

            // Assert
        }

        [Test]
        public void ValidateUniquenessForRoomNumberWithNoRoomThrowsNoException()
        {
            // Arrange
            List<Room> rooms = new List<Room>();

            // Act
            this.roomNumberValidator.ValidateUniquenessForRoomNumber(rooms);

            // Assert
        }

        [Test]
        public void ValidateUniquenessForRoomNumberWithOneRoomThrowsNoException()
        {
            // Arrange
            List<Room> rooms = new List<Room>();
            Room firstRoom = new Room();
            firstRoom.RoomNumber = "1111";
            rooms.Add(firstRoom);

            // Act
            this.roomNumberValidator.ValidateUniquenessForRoomNumber(rooms);

            // Assert
        }

        [Test]
        public void ValidateUniquenessForRoomNumberWithRoomNumberIsSeveralThrowsException()
        {
            // Arrange
            Room firstRoom = new Room { RoomNumber = "1111" };
            Room secondRoom = new Room { RoomNumber = "1121" };
            Room thirdRoom = new Room { RoomNumber = "1111" };
            Room fourthRoom = new Room { RoomNumber = "1111" };
            Room fifthRoom = new Room { RoomNumber = "4444" };
            Room sixthRoom = new Room { RoomNumber = "1121" };

            List<Room> rooms = new List<Room>
            {
                firstRoom,
                secondRoom,
                thirdRoom,
                fourthRoom,
                fifthRoom,
                sixthRoom,
            };

            // Act
            NonUniqueRoomException ex =
                Assert.Throws<NonUniqueRoomException>(
                    () => this.roomNumberValidator.ValidateUniquenessForRoomNumber(rooms));

            // Assert
            Assert.That(
                ex.Message,
                Is.EqualTo("The file should contains only unique rooms.The room numbers are not unique: 1111, 1121"));
        }

        [Test]
        public void ValidateUniquenessForRoomNumberWithRoomNumberIsTwiceThrowsException()
        {
            // Arrange
            Room firstRoom = new Room { RoomNumber = "1111" };
            Room secondRoom = new Room { RoomNumber = "1121" };
            Room thirdRoom = new Room { RoomNumber = "1141" };
            Room fourthRoom = new Room { RoomNumber = "1111" };
            List<Room> rooms = new List<Room> { firstRoom, secondRoom, thirdRoom, fourthRoom };

            // Act
            NonUniqueRoomException ex =
                Assert.Throws<NonUniqueRoomException>(
                    () => this.roomNumberValidator.ValidateUniquenessForRoomNumber(rooms));

            // Assert
            Assert.That(
                ex.Message,
                Is.EqualTo("The file should contains only unique rooms.The room numbers are not unique: 1111"));
        }

        [Test]
        public void ValidateUniquenessForRoomNumberWithRoomNumbersAreDifferentThrowsNoException()
        {
            // Arrange
            List<Room> rooms = new List<Room>();
            Room firstRoom = new Room();
            firstRoom.RoomNumber = "1111";
            rooms.Add(firstRoom);
            Room secondRoom = new Room();
            secondRoom.RoomNumber = "2222";
            rooms.Add(secondRoom);

            // Act
            this.roomNumberValidator.ValidateUniquenessForRoomNumber(rooms);

            // Assert
        }
    }
}
