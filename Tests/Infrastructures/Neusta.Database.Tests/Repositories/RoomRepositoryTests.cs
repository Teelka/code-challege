﻿namespace Neusta.Database.Tests.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using Neusta.Database.Entities;
    using Neusta.Database.Repositories;
    using NUnit.Framework;

    [TestFixture]
    public class RoomRepositoryTests : TestBaseRepository
    {
        private RoomRepository roomRepository;

        [SetUp]
        public void SetUp()
        {
            this.roomRepository = new RoomRepository(this.context);
        }

        [Test]
        public void GetAllRoomsReturnsAllRooms()
        {
            // Arrange
            this.AddRoom("1111", null);

            // Act
            List<Room> result = this.roomRepository.GetAllRooms();

            // Assert
            Assert.That(result, Is.Not.Empty);
            Assert.That(result.Count, Is.EqualTo(1));
            Assert.That(result[0].RoomNumber, Is.EqualTo("1111"));
        }

        [Test]
        public void GetAllRoomsReturnsAllRoomsWithPeople()
        {
            // Arrange
            this.AddRoom("4444", createPeople("temkes"));

            // Act
            List<Room> result = this.roomRepository.GetAllRooms();

            // Assert
            Assert.That(result[0].People, Is.Not.Empty);
            Assert.That(result[0].People.Count, Is.EqualTo(1));
            Assert.That(result[0].People[0].Ldapuser, Is.EqualTo("temkes"));
        }

        [Test]
        public void FindRoomWithCorrectRoomNumberReturnsRoom()
        {
            // Arrange
            this.AddRoom("4444", null);

            // Act
            Room result = this.roomRepository.FindRoom("4444");

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.RoomNumber, Is.EqualTo("4444"));
        }

        [Test]
        public void FindRoomWithCorrectRoomNumberReturnsRoomWithPeople()
        {
            // Arrange
            this.AddRoom("4444", createPeople("temkes"));

            // Act
            Room result = this.roomRepository.FindRoom("4444");

            // Assert
            Assert.That(result.People, Is.Not.Empty);
            Assert.That(result.People.Count, Is.EqualTo(1));
            Assert.That(result.People[0].Ldapuser, Is.EqualTo("temkes"));
        }

        [Test]
        public void FindRoomWithWrongRoomNumberReturnsNull()
        {
            // Arrange
            this.AddRoom("4444", null);

            // Act
            Room result = this.roomRepository.FindRoom("1111");

            // Assert
            Assert.That(result, Is.Null);
        }

        [Test]
        public void SaveCallsRemoveRangePeople()
        {
            // Arrange
            this.AddRoom("4444", createPeople("temkes"));

            // Act
            this.roomRepository.Save(new List<Room>());

            // Assert
            Assert.That(this.context.People.ToList(), Is.Empty);
        }

        [Test]
        public void SaveCallsRemoveRangeRooms()
        {
            // Arrange
            this.AddRoom("4444", createPeople("temkes"));

            // Act
            this.roomRepository.Save(new List<Room>());

            // Assert
            Assert.That(this.context.Rooms.ToList(), Is.Empty);
        }

        [Test]
        public void SaveSaveAllRooms()
        {
            // Arrange
            Room firstRoom = new Room { RoomNumber = "4444", People = null };
            Room secondRoom = new Room { RoomNumber = "5555", People = null };
            List<Room> rooms = new List<Room> { firstRoom, secondRoom };

            // Act
            this.roomRepository.Save(rooms);

            // Assert
            List<Room> actualRooms = this.context.Rooms.ToList();
            Assert.That(actualRooms, Is.Not.Empty);
            Assert.That(actualRooms.Count, Is.EqualTo(2));
            Assert.That(actualRooms[0].RoomNumber, Is.EqualTo("4444"));
            Assert.That(actualRooms[1].RoomNumber, Is.EqualTo("5555"));
        }

        [Test]
        public void SaveSaveAllRoomsWithPeople()
        {
            // Arrange
            Room firstRoom = new Room { RoomNumber = "4444", People = createPeople("temkes", "bla") };
            List<Room> rooms = new List<Room> { firstRoom };

            // Act
            this.roomRepository.Save(rooms);

            // Assert
            List<Room> actualRooms = this.context.Rooms.ToList();
            Assert.That(actualRooms[0].People, Is.Not.Empty);
            Assert.That(actualRooms[0].People.Count, Is.EqualTo(2));
            Assert.That(actualRooms[0].People[0].Ldapuser, Is.EqualTo("temkes"));
            Assert.That(actualRooms[0].People[1].Ldapuser, Is.EqualTo("bla"));
        }

        private void AddRoom(string roomNumber, List<Person> people)
        {
            Room room = new Room { RoomNumber = roomNumber, People = people };
            this.context.Rooms.Add(room);
            this.context.SaveChanges();
        }

        private static List<Person> createPeople(params string[] ldapUsers)
        {
            return ldapUsers.Select(ldapUser => new Person { Ldapuser = ldapUser }).ToList();
        }
    }
}
