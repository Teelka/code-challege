﻿namespace Neusta.Database.Tests.Repositories
{
    using System.Data.Common;
    using Microsoft.Data.Sqlite;
    using Microsoft.EntityFrameworkCore;
    using Neusta.Database.Contexts;
    using NUnit.Framework;

    public abstract class TestBaseRepository
    {
        protected SitzplanDbContext context;
        private DbConnection connection;

        /// different database solutions:
        /// https://github.com/dotnet/EntityFramework.Docs/tree/master/samples/core/Miscellaneous/Testing/ItemsWebApi/Tests
        [SetUp]
        public void BaseSetUp()
        {
            DbContextOptions<SitzplanDbContext> options = new DbContextOptionsBuilder<SitzplanDbContext>()
                .UseSqlite(this.CreateInMemoryDatabase())
                .Options;

            this.context = new SitzplanDbContext(options);
            this.context.Database.EnsureDeleted();
            this.context.Database.EnsureCreated();
        }

        [TearDown]
        public void BaseTearDown()
        {
            this.connection?.Dispose();
        }

        private DbConnection CreateInMemoryDatabase()
        {
            this.connection = new SqliteConnection("Filename=:memory:");
            this.connection.Open();
            return this.connection;
        }
    }
}
