#!/bin/sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
DIR=$(dirname "$SCRIPT")
cd $DIR/..


IMAGE_BACKEND_NAME=teelka/code-challenge-backend
IMAGE_FRONTEND_NAME=teelka/code-challenge-frontend

docker build -t $IMAGE_BACKEND_NAME:latest -f ./src/Applications/Neusta.Sitzplan.Api/Dockerfile .
docker login --username=teelka 
docker push $IMAGE_BACKEND_NAME:latest

docker build -t $IMAGE_FRONTEND_NAME:latest -f ./src/Applications/Neusta.Sitzplan.Blazor/Dockerfile .
docker login --username=teelka 
docker push $IMAGE_FRONTEND_NAME:latest