﻿namespace Neusta.Sitzplan.Api.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Neusta.Sitzplan.Converters;
    using Neusta.Sitzplan.Services;

    [ApiController]
    [Route("api/[controller]")]
    public class ImportController : ControllerBase
    {
        private readonly ICsvToStringConverter csvToStringConverter;
        private readonly IImportService importService;
        private readonly ILogger<ImportController> logger;

        public ImportController(
            IImportService importService,
            ILogger<ImportController> logger,
            ICsvToStringConverter csvToStringConverter)
        {
            this.importService = importService;
            this.logger = logger;
            this.csvToStringConverter = csvToStringConverter;
        }

        /// <summary>
        ///     Imports a csv file and saves the rooms including the people.
        /// </summary>
        /// <param name="importFile">A csv file with a Sitzplan with rooms and people in the rooms.</param>
        /// <returns> success or exception. </returns>
        [HttpPost]
        public async Task<IActionResult> ImportRoomsAsync(IFormFile importFile)
        {
            this.logger.LogInformation("Starts to import the csv file");
            List<string> rooms =
                await this.csvToStringConverter.GetContentFromFileAsync(importFile).ConfigureAwait(false);
            this.importService.ImportRooms(rooms);
            this.logger.LogInformation("Ended to import the csv file");
            return this.Ok();
        }
    }
}
