﻿namespace Neusta.Sitzplan.Api.Controllers
{
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Mvc;
    using Neusta.Sitzplan.DTOs;
    using Neusta.Sitzplan.Services;

    [ApiController]
    [Route("api/[controller]")]
    public class RoomController : ControllerBase
    {
        private readonly IRoomService roomService;

        public RoomController(IRoomService roomService)
        {
            this.roomService = roomService;
        }

        /// <summary>
        ///     Get all rooms including the people in the rooms from the Sitzplan.
        /// </summary>
        /// <returns> A List of Rooms.</returns>
        [HttpGet]
        public List<RoomDto> Get()
        {
            return this.roomService.GetAllRooms();
        }

        /// <summary>
        ///     Get one room including the people in the room from the Sitzplan.
        /// </summary>
        /// <param name="number"> the room number</param>
        /// <returns>An existing room or throws an exception, if the room doesn't exist.</returns>
        [HttpGet("{number}")]
        public RoomDto Get(string number)
        {
            return this.roomService.GetRoom(number);
        }
    }
}
