﻿namespace Neusta.Sitzplan.Api.Middleware
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Text.Json;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Neusta.Sitzplan.Api.Models;
    using Neusta.Sitzplan.Enums;
    using Neusta.Sitzplan.Exceptions;

    [ExcludeFromCodeCoverage]
    public class ExceptionHandlerMiddleware
    {
        private const string ResponseContentType = "application/problem+json";
        private readonly IDictionary<Type, Func<HttpContext, Exception, Task>> exceptionHandlers;

        private readonly RequestDelegate next;

        private IWebHostEnvironment hostEnvironment;

        private ILogger<ExceptionHandlerMiddleware> logger;

        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            this.next = next;

            // Register known exception types and handlers.
            this.exceptionHandlers = new Dictionary<Type, Func<HttpContext, Exception, Task>>
            {
                { typeof(RoomNotFoundException), this.HandleRoomNotFoundException },
                { typeof(InvalidCsvDataException), this.HandleInvalidFileData },
                { typeof(NonFourDigitRoomNumberException), this.HandleNonFourDigitRoom },
                { typeof(NonUniqueRoomException), this.HandleNonUniqueRoom },
                { typeof(NonUniquePersonException), this.HandleNonUniquePerson },
            };
        }

        public async Task Invoke(
            HttpContext context,
            IWebHostEnvironment webHostEnvironment,
            ILogger<ExceptionHandlerMiddleware> exceptionHandlerMiddlewareLogger)
        {
            try
            {
                await this.next(context);
            }
            catch (Exception exception)
            {
                this.hostEnvironment = webHostEnvironment;
                this.logger = exceptionHandlerMiddlewareLogger;
                await this.HandleExceptionAsync(context, exception);
            }
        }

        private Task HandleRoomNotFoundException(HttpContext context, Exception ex)
        {
            if (!(ex is RoomNotFoundException exception))
            {
                return this.HandleUnknownException(context, ex);
            }

            context.Response.ContentType = ResponseContentType;
            context.Response.StatusCode = StatusCodes.Status404NotFound;

            ErrorResponse details = new ErrorResponse { Code = (int)ErrorCode.NoRoom, Message = exception.Message };
            this.logger.LogError(exception, exception.Message);
            return context.Response.WriteAsync(SerializeObject(details));
        }

        private Task HandleInvalidFileData(HttpContext context, Exception ex)
        {
            if (!(ex is InvalidCsvDataException exception))
            {
                return this.HandleUnknownException(context, ex);
            }

            context.Response.ContentType = ResponseContentType;
            context.Response.StatusCode = StatusCodes.Status400BadRequest;

            ErrorResponse details =
                new ErrorResponse { Code = (int)ErrorCode.ValidCsvLine, Message = exception.Message };
            this.logger.LogError(exception, exception.Message);

            return context.Response.WriteAsync(SerializeObject(details));
        }

        private Task HandleNonFourDigitRoom(HttpContext context, Exception ex)
        {
            if (!(ex is NonFourDigitRoomNumberException exception))
            {
                return this.HandleUnknownException(context, ex);
            }

            context.Response.ContentType = ResponseContentType;
            context.Response.StatusCode = StatusCodes.Status400BadRequest;

            ErrorResponse details = new ErrorResponse
            {
                Code = (int)ErrorCode.NonFourDigitRoomNumber, Message = exception.Message,
            };
            this.logger.LogError(exception, exception.Message);

            return context.Response.WriteAsync(SerializeObject(details));
        }

        private Task HandleNonUniqueRoom(HttpContext context, Exception ex)
        {
            if (!(ex is NonUniqueRoomException exception))
            {
                return this.HandleUnknownException(context, ex);
            }

            context.Response.ContentType = ResponseContentType;
            context.Response.StatusCode = StatusCodes.Status400BadRequest;

            ErrorResponse details =
                new ErrorResponse { Code = (int)ErrorCode.NonUniqueRoom, Message = exception.Message };
            this.logger.LogError(exception, exception.Message);

            return context.Response.WriteAsync(SerializeObject(details));
        }

        private Task HandleNonUniquePerson(HttpContext context, Exception ex)
        {
            if (!(ex is NonUniquePersonException exception))
            {
                return this.HandleUnknownException(context, ex);
            }

            context.Response.ContentType = ResponseContentType;
            context.Response.StatusCode = StatusCodes.Status400BadRequest;

            ErrorResponse details =
                new ErrorResponse { Code = (int)ErrorCode.NonUniquePerson, Message = exception.Message };
            this.logger.LogError(exception, exception.Message);

            return context.Response.WriteAsync(SerializeObject(details));
        }

        private static string SerializeObject(ErrorResponse errorResponse)
        {
            return JsonSerializer.Serialize(errorResponse);
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            Type type = exception.GetType();
            if (this.exceptionHandlers.ContainsKey(type))
            {
                return this.exceptionHandlers[type].Invoke(context, exception);
            }

            return this.HandleUnknownException(context, exception);
        }

        private Task HandleUnknownException(HttpContext context, Exception exception)
        {
            context.Response.ContentType = ResponseContentType;
            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            string exceptionUrn = $"urn:nat:error:{context.TraceIdentifier}";

            string errorDetail =
                $"Oops an error occurred! Please contact support with the following error code for further information: {exceptionUrn}";
            if (this.hostEnvironment.IsDevelopment())
            {
                errorDetail = exception.ToString();
            }

            ErrorResponse details = new ErrorResponse { Code = 0, Message = errorDetail };

            this.logger.LogError(exception, $"{exceptionUrn}: {exception.Message}");

            return context.Response.WriteAsync(SerializeObject(details));
        }
    }
}
