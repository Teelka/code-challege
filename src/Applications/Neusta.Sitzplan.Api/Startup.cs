﻿namespace Neusta.Sitzplan.Api
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Reflection;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.OpenApi.Models;
    using Neusta.Database.Contexts;
    using Neusta.Database.Repositories;
    using Neusta.Sitzplan.Api.Middleware;
    using Neusta.Sitzplan.Converters;
    using Neusta.Sitzplan.Mappers;
    using Neusta.Sitzplan.Parsers;
    using Neusta.Sitzplan.Services;
    using Neusta.Sitzplan.Validators;

    [ExcludeFromCodeCoverage]
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddScoped<IRoomService, RoomService>();
            services.AddScoped<IRoomMapper, RoomMapper>();
            services.AddScoped<IRoomRepository, RoomRepository>();
            services.AddScoped<IImportService, ImportService>();
            services.AddScoped<IFileValidator, FileValidator>();
            services.AddScoped<IRoomNumberValidator, RoomNumberValidator>();
            services.AddScoped<IPersonValidator, PersonValidator>();
            services.AddScoped<IStringToPersonParser, StringToPersonParser>();
            services.AddScoped<IStringToRoomParser, StringToRoomParser>();
            services.AddScoped<ICsvToStringConverter, CsvToStringConverter>();

            services.AddDbContext<SitzplanDbContext>(
                options => options.UseNpgsql(this.Configuration.GetConnectionString("DefaultConnection")));

            services.AddSwaggerGen(
                options =>
                {
                    options.SwaggerDoc(
                        "v1",
                        new OpenApiInfo
                        {
                            Title = "Swagger ApPI", Description = "The API for Swagger", Version = "v1",
                        });
                    string xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    string xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                    options.IncludeXmlComments(xmlPath);
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ExceptionHandlerMiddleware>();


            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(
                endpoints =>
                {
                    endpoints.MapControllers();
                });
            app.UseSwagger();
            app.UseSwaggerUI(
                options =>
                {
                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "Swagger Api");
                    options.RoutePrefix = string.Empty;
                });
            MigrateDb(app);
        }

        private static void MigrateDb(IApplicationBuilder app)
        {
            using (IServiceScope serviceScope =
                app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                using (SitzplanDbContext context = serviceScope.ServiceProvider.GetService<SitzplanDbContext>())
                {
                    context.Database.Migrate();
                }
            }
        }
    }
}
