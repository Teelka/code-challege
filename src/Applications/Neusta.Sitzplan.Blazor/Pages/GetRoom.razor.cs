﻿namespace Neusta.Sitzplan.Blazor.Pages
{
    using System.Text.Json;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
    using Neusta.Sitzplan.Api.Models;
    using Neusta.Sitzplan.Blazor.Services;
    using Neusta.Sitzplan.Blazor.SitzplanApiService;

    public partial class GetRoom
    {
        private RoomDto room;

        private bool loadFailed;

        private string errorMessage;

        [Inject]
        private IRoomService roomService { get; set; }

        private string RoomNumber { get; set; }

        private async Task FindRoom()
        {
            try
            {
                this.loadFailed = false;
                this.room = await this.roomService.GetRoom(this.RoomNumber);
            }
            catch (ApiException apiException)
            {
                this.loadFailed = true;
                this.room = null;
                this.errorMessage = DefineErrorMessage(apiException);
            }
        }

        private static string DefineErrorMessage(ApiException apiException)
        {
            try
            {
                string response = apiException.Response;
                ErrorResponse error = JsonSerializer.Deserialize<ErrorResponse>(response);
                if (error.Code == 5)
                {
                    return "Es gibt keinen Raum mit der genannten Raumnummer.";
                }

                return "Ein unbekannter Fehler ist passiert.";
            }
            catch (JsonException jsonException)
            {
                return "Ein unbekannter Fehler ist passiert.";
            }
        }
    }
}
