﻿namespace Neusta.Sitzplan.Blazor.Pages
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
    using Neusta.Sitzplan.Blazor.Services;
    using Neusta.Sitzplan.Blazor.SitzplanApiService;

    public partial class GetRooms
    {
        private List<RoomDto> rooms;

        [Inject]
        private IRoomService roomService { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.rooms = await this.roomService.GetRooms();
        }
    }
}
