﻿namespace Neusta.Sitzplan.Blazor.Pages
{
    using System.Linq;
    using System.Text.Json;
    using System.Threading.Tasks;
    using BlazorInputFile;
    using Microsoft.AspNetCore.Components;
    using Neusta.Sitzplan.Api.Models;
    using Neusta.Sitzplan.Blazor.Services;
    using Neusta.Sitzplan.Blazor.SitzplanApiService;

    public partial class Import
    {
        private IFileListEntry file;
        private string message;

        [Inject]
        public IImportService ImportService { get; set; }

        private async Task ImportSitzplan()
        {
            if (this.file != null)
            {
                try
                {
                    await this.ImportService.ImportFile(this.file);
                    this.message = $"Du hast die File {this.file.Name} importiert.";
                }
                catch (ApiException apiException)
                {
                    this.message = DefineErrorMessage(apiException);
                }
            }
        }

        private static string DefineErrorMessage(ApiException apiException)
        {
            try
            {
                string response = apiException.Response;
                ErrorResponse error = JsonSerializer.Deserialize<ErrorResponse>(response);
                switch (error.Code)
                {
                    case 2: return "Eine Raumnummer ist doppelt. Die File konnte nicht importiert werden.";
                    case 3: return "Eine Person ist doppelt. Die File konnte nicht importiert werden.";
                    case 4: return "Eine Person hat keinen validen Namen. Die File konnte nicht importiert werden.";
                    case 6: return "Eine Raumnummer ist nicht vierstellig. Die File konnte nicht importiert werden.";
                    default: return "Ein unbekannter Fehler ist passiert.";
                }
            }
            catch (JsonException jsonException)
            {
                return "Ein unbekannter Fehler ist passiert.";
            }
        }

        private void HandleSelection(IFileListEntry[] files)
        {
            this.file = files.FirstOrDefault();
        }
    }
}
