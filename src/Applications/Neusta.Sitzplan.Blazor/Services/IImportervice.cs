﻿namespace Neusta.Sitzplan.Blazor.Services
{
    using System.Threading.Tasks;
    using BlazorInputFile;

    public interface IImportService
    {
        Task ImportFile(IFileListEntry importFile);
    }
}
