﻿namespace Neusta.Sitzplan.Blazor.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Neusta.Sitzplan.Blazor.SitzplanApiService;

    public interface IRoomService
    {
        Task<List<RoomDto>> GetRooms();

        Task<RoomDto> GetRoom(string roomNumber);
    }
}
