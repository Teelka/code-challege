﻿namespace Neusta.Sitzplan.Blazor.Services
{
    using System.IO;
    using System.Net.Http;
    using System.Threading.Tasks;
    using BlazorInputFile;
    using Neusta.Sitzplan.Blazor.SitzplanApiService;

    public class ImportService : IImportService
    {
        private readonly SitzplanApi openApiClient;

        public ImportService(SitzplanApi openApiClient)
        {
            this.openApiClient = openApiClient;
        }

        public async Task ImportFile(IFileListEntry importFile)
        {
            MemoryStream ms = new MemoryStream();
            await importFile.Data.CopyToAsync(ms);
            byte[] content = ms.ToArray();
            ByteArrayContent byteArrayContent = new ByteArrayContent(content);
            Stream asStreamAsync = await byteArrayContent.ReadAsStreamAsync();
            FileParameter fileParameter = new FileParameter(asStreamAsync);
            await this.openApiClient.ApiImportAsync(fileParameter);
        }
    }
}
