﻿namespace Neusta.Sitzplan.Blazor.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Neusta.Sitzplan.Blazor.SitzplanApiService;

    public class RoomService : IRoomService
    {
        private readonly SitzplanApi sitzplanApi;

        public RoomService(SitzplanApi sitzplanApi)
        {
            this.sitzplanApi = sitzplanApi;
        }

        public async Task<RoomDto> GetRoom(string roomNumber)
        {
            return await this.sitzplanApi.ApiRoomGetAsync(roomNumber);
        }

        public async Task<List<RoomDto>> GetRooms()
        {
            return await this.sitzplanApi.ApiRoomGetAsync();
        }
    }
}
