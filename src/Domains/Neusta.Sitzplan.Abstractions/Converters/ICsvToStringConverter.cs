namespace Neusta.Sitzplan.Converters
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;

    public interface ICsvToStringConverter
    {
        Task<List<string>> GetContentFromFileAsync(IFormFile csvFile);
    }
}
