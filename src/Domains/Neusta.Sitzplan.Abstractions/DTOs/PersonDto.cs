﻿namespace Neusta.Sitzplan.DTOs
{
    using System.Text.Json.Serialization;

    public class PersonDto
    {
        [JsonPropertyName("first name")]
        public string FirstName { get; set; }

        [JsonPropertyName("last name")]
        public string LastName { get; set; }

        [JsonPropertyName("title")]
        public string Title { get; set; }

        [JsonPropertyName("name addition")]
        public string NameAddition { get; set; }

        [JsonPropertyName("ldapuser")]
        public string Ldapuser { get; set; }
    }
}
