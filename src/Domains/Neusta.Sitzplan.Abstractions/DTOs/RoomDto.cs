﻿namespace Neusta.Sitzplan.DTOs
{
    using System.Collections.Generic;
    using System.Text.Json.Serialization;

    public class RoomDto
    {
        [JsonPropertyName("room")]
        public string RoomNumber { get; set; }

        public List<PersonDto> People { get; set; }
    }
}
