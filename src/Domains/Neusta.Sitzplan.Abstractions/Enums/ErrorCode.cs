﻿namespace Neusta.Sitzplan.Enums
{
    public enum ErrorCode : ushort
    {
        NonUniqueRoom = 2,
        NonUniquePerson = 3,
        ValidCsvLine = 4,
        NoRoom = 5,
        NonFourDigitRoomNumber = 6,
    }
}
