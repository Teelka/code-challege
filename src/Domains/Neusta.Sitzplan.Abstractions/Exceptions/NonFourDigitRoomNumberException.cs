﻿namespace Neusta.Sitzplan.Exceptions
{
    using System;

    public class NonFourDigitRoomNumberException : Exception
    {
        public NonFourDigitRoomNumberException(string message)
            : base(message)
        {
        }
    }
}
