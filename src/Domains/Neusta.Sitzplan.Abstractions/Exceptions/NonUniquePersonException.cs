﻿namespace Neusta.Sitzplan.Exceptions
{
    using System;

    public class NonUniquePersonException : Exception
    {
        public NonUniquePersonException(string message)
            : base(message)
        {
        }
    }
}
