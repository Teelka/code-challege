﻿namespace Neusta.Sitzplan.Exceptions
{
    using System;

    public class NonUniqueRoomException : Exception
    {
        public NonUniqueRoomException(string message)
            : base(message)
        {
        }
    }
}
