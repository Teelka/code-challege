﻿namespace Neusta.Sitzplan.Exceptions
{
    using System;

    public class RoomNotFoundException : Exception
    {
        public RoomNotFoundException(string message)
            : base(message)
        {
        }
    }
}
