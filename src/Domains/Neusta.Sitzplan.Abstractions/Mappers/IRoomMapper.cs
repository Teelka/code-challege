﻿namespace Neusta.Sitzplan.Mappers
{
    using Neusta.Database.Entities;
    using Neusta.Sitzplan.DTOs;

    public interface IRoomMapper
    {
        RoomDto MapRoomToRoomDto(Room room);
    }
}
