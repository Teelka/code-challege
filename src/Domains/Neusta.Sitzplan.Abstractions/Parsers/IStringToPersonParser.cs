﻿namespace Neusta.Sitzplan.Parsers
{
    using Neusta.Database.Entities;

    public interface IStringToPersonParser
    {
        Person ParseStringToPerson(string person);
    }
}
