﻿namespace Neusta.Sitzplan.Parsers
{
    using Neusta.Database.Entities;

    public interface IStringToRoomParser
    {
        Room ParseStringToRoom(string room);
    }
}
