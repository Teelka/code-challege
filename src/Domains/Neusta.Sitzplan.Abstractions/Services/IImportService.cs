﻿namespace Neusta.Sitzplan.Services
{
    using System.Collections.Generic;

    public interface IImportService
    {
        void ImportRooms(List<string> rooms);
    }
}
