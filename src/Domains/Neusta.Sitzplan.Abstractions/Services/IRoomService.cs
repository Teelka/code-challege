﻿namespace Neusta.Sitzplan.Services
{
    using System.Collections.Generic;
    using Neusta.Sitzplan.DTOs;

    public interface IRoomService
    {
        List<RoomDto> GetAllRooms();

        RoomDto GetRoom(string roomNumber);
    }
}
