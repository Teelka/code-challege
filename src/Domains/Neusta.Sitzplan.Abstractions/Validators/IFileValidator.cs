﻿namespace Neusta.Sitzplan.Validators
{
    using Microsoft.AspNetCore.Http;

    public interface IFileValidator
    {
        void ValidateFile(IFormFile inputFile);
    }
}
