﻿namespace Neusta.Sitzplan.Validators
{
    using System.Collections.Generic;
    using Neusta.Database.Entities;

    public interface IPersonValidator
    {
        void ValidateUniquenessForPeople(List<Room> lists);
    }
}
