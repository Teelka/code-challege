﻿namespace Neusta.Sitzplan.Validators
{
    using System.Collections.Generic;
    using Neusta.Database.Entities;

    public interface IRoomNumberValidator
    {
        void ValidateRoomNumber(string roomNumber);

        void ValidateUniquenessForRoomNumber(List<Room> rooms);
    }
}
