﻿namespace Neusta.Sitzplan.Constants
{
    using System.Collections.Generic;

    public class RoomConstants
    {
        public static readonly string EntriesSeparator = ",";

        public static readonly string PersonNamesSeparator = " ";

        public static readonly string PersonTitle = "Dr.";

        public static readonly char BracketBeforeLdapuser = '(';

        public static readonly char BracketAfterLdapuser = ')';

        public static readonly List<string> PersonNameAdditions = new List<string> { "de", "van", "von" };
    }
}
