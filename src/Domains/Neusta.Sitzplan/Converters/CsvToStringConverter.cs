﻿namespace Neusta.Sitzplan.Converters
{
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Neusta.Sitzplan.Validators;

    public class CsvToStringConverter : ICsvToStringConverter
    {
        private readonly IFileValidator fileValidator;

        public CsvToStringConverter(IFileValidator fileValidator)
        {
            this.fileValidator = fileValidator;
        }

        public async Task<List<string>> GetContentFromFileAsync(IFormFile csvFile)
        {
            this.fileValidator.ValidateFile(csvFile);
            return await GetContentList(csvFile);
        }

        private static async Task<List<string>> GetContentList(IFormFile csvFile)
        {
            List<string> content = new List<string>();
            using StreamReader reader = new StreamReader(csvFile.OpenReadStream());
            while (reader.Peek() >= 0)
            {
                content.Add(await reader.ReadLineAsync());
            }

            return content;
        }
    }
}
