﻿namespace Neusta.Sitzplan.Mappers
{
    using System.Collections.Generic;
    using System.Linq;
    using Neusta.Database.Entities;
    using Neusta.Sitzplan.DTOs;

    public class RoomMapper : IRoomMapper
    {
        public RoomDto MapRoomToRoomDto(Room room)
        {
            RoomDto roomDto = new RoomDto { RoomNumber = room.RoomNumber };
            List<PersonDto> people = null;
            if (room.People != null)
            {
                people = room.People.Select(
                        person => new PersonDto
                        {
                            FirstName = person.FirstName,
                            LastName = person.LastName,
                            Title = person.Title,
                            NameAddition = person.NameAddition,
                            Ldapuser = person.Ldapuser,
                        })
                    .ToList();
            }

            roomDto.People = people;
            return roomDto;
        }
    }
}
