﻿namespace Neusta.Sitzplan.Parsers
{
    using System.Linq;
    using System.Reflection;
    using System.Resources;
    using Neusta.Database.Entities;
    using Neusta.Sitzplan.Constants;
    using Neusta.Sitzplan.Exceptions;

    public class StringToPersonParser : IStringToPersonParser
    {
        private static readonly ResourceManager ResourceManager = new ResourceManager(
            "Neusta.Sitzplan.Resources.ExceptionMessages",
            Assembly.GetExecutingAssembly());

        public Person ParseStringToPerson(string personAsString)
        {
            string[] personNames = personAsString.Split(RoomConstants.PersonNamesSeparator);

            Person person = new Person();
            ValidateMinEntriesFromPersonNames(personNames, personAsString);
            person.Title = GetTitle(personNames);
            personNames = CutTitleFromPersonNames(personNames);
            ValidateMinEntriesFromPersonNames(personNames, personAsString);
            ValidateLdapuser(personNames, personAsString);
            person.Ldapuser = GetLdapuser(personNames);
            personNames = CutLdapuserFromPersonNames(personNames);
            person.LastName = GetLastName(personNames);
            personNames = CutLastNameFromPersonNames(personNames);
            ValidateMaxNumberOfFirstNamesAndNameAdditions(personNames, personAsString);
            person.NameAddition = GetNameAddition(personNames);
            personNames = CutNameAdditionFromPersonNames(personNames, person.NameAddition);
            person.FirstName = GetFirstName(personNames, personAsString);

            return person;
        }

        private static string GetNameAddition(string[] personNames)
        {
            string possibleNameAddition = personNames[^1];
            if (HasNameAddition(personNames, possibleNameAddition))
            {
                return possibleNameAddition;
            }

            return string.Empty;
        }

        private static bool HasNameAddition(string[] personNames, string possibleNameAddition)
        {
            return personNames.Length > 1 && RoomConstants.PersonNameAdditions.Contains(possibleNameAddition);
        }

        private static string[] CutNameAdditionFromPersonNames(string[] personValues, string nameAddition)
        {
            if (HasNameAddition(personValues, nameAddition))
            {
                return personValues.SkipLast(1).ToArray();
            }

            return personValues;
        }

        private static string GetFirstName(string[] personNames, string personAsString)
        {
            ValidateMaxNumberOfFirstNames(personNames, personAsString);

            return string.Join(" ", personNames);
        }

        private static void ValidateMaxNumberOfFirstNamesAndNameAdditions(string[] personValues, string personAsString)
        {
            if (personValues.Length > 3)
            {
                string message = string.Format(
                    ResourceManager.GetString("NotValidPersonName") ?? string.Empty,
                    personAsString);
                throw new InvalidCsvDataException(message);
            }
        }

        private static void ValidateMaxNumberOfFirstNames(string[] personValues, string personAsString)
        {
            if (personValues.Length > 2)
            {
                string message = string.Format(
                    ResourceManager.GetString("NotValidPersonName") ?? string.Empty,
                    personAsString);
                throw new InvalidCsvDataException(message);
            }
        }

        private static void ValidateLdapuser(string[] personValues, string personAsString)
        {
            string ldapuserWithBrackets = personValues[^1];
            if (!ldapuserWithBrackets.Contains(RoomConstants.BracketBeforeLdapuser) ||
                !ldapuserWithBrackets.Contains(RoomConstants.BracketAfterLdapuser))
            {
                string message = string.Format(
                    ResourceManager.GetString("NotValidPersonName") ?? string.Empty,
                    personAsString);
                throw new InvalidCsvDataException(message);
            }
        }

        private static void ValidateMinEntriesFromPersonNames(string[] personValues, string personAsString)
        {
            if (personValues.Length < 3)
            {
                string message = string.Format(
                    ResourceManager.GetString("NotValidPersonName") ?? string.Empty,
                    personAsString);
                throw new InvalidCsvDataException(message);
            }
        }

        private static string[] CutLastNameFromPersonNames(string[] personValues)
        {
            return personValues.SkipLast(1).ToArray();
        }

        private static string GetLastName(string[] personValues)
        {
            return personValues[^1];
        }

        private static string[] CutLdapuserFromPersonNames(string[] personValues)
        {
            return personValues.SkipLast(1).ToArray();
        }

        private static string GetLdapuser(string[] personValues)
        {
            return personValues[^1].Trim(RoomConstants.BracketBeforeLdapuser).Trim(RoomConstants.BracketAfterLdapuser);
        }

        private static string[] CutTitleFromPersonNames(string[] personValues)
        {
            string[] newPersonValues = personValues;
            if (HasTitle(personValues))
            {
                newPersonValues = personValues.Skip(1).ToArray();
            }

            return newPersonValues;
        }

        private static string GetTitle(string[] personValues)
        {
            return HasTitle(personValues)
                ? personValues[0]
                : string.Empty;
        }

        private static bool HasTitle(string[] personValues)
        {
            return personValues[0] == RoomConstants.PersonTitle;
        }
    }
}
