﻿namespace Neusta.Sitzplan.Parsers
{
    using System.Collections.Generic;
    using System.Linq;
    using Neusta.Database.Entities;
    using Neusta.Sitzplan.Constants;
    using Neusta.Sitzplan.Validators;

    public class StringToRoomParser : IStringToRoomParser
    {
        private readonly IRoomNumberValidator roomNumberValidator;
        private readonly IStringToPersonParser stringToPersonParser;

        public StringToRoomParser(IRoomNumberValidator roomNumberValidator, IStringToPersonParser stringToPersonParser)
        {
            this.roomNumberValidator = roomNumberValidator;
            this.stringToPersonParser = stringToPersonParser;
        }

        public Room ParseStringToRoom(string roomAsString)
        {
            string[] entriesFromRoom = roomAsString.Split(RoomConstants.EntriesSeparator);
            Room room = new Room();
            string roomNumber = entriesFromRoom[0];
            this.ParseRoomNumber(roomNumber, room);

            string[] peopleEntries = SkipRoomNumber(entriesFromRoom);
            room.People = this.ParseStringArrayToPeople(peopleEntries);

            return room;
        }

        private static string[] SkipRoomNumber(string[] entriesFromRoom)
        {
            return entriesFromRoom.Skip(1).ToArray();
        }

        private List<Person> ParseStringArrayToPeople(string[] peopleAsStrings)
        {
            List<Person> people =
                peopleAsStrings
                    .Where(personAsString => personAsString != string.Empty)
                    .Select(personAsString => this.stringToPersonParser.ParseStringToPerson(personAsString))
                    .ToList();

            if (!people.Any())
            {
                people = null;
            }

            return people;
        }

        private void ParseRoomNumber(string roomNumber, Room room)
        {
            this.roomNumberValidator.ValidateRoomNumber(roomNumber);
            room.RoomNumber = roomNumber;
        }
    }
}
