﻿namespace Neusta.Sitzplan.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Neusta.Database.Entities;
    using Neusta.Database.Repositories;
    using Neusta.Sitzplan.Parsers;
    using Neusta.Sitzplan.Validators;

    public class ImportService : IImportService
    {
        private readonly IPersonValidator personValidator;
        private readonly IStringToRoomParser stringToRoomParser;
        private readonly IRoomRepository roomRepository;
        private readonly IRoomNumberValidator roomNumberValidator;

        public ImportService(
            IRoomNumberValidator roomNumberValidator,
            IRoomRepository roomRepository,
            IStringToRoomParser stringToRoomParser,
            IPersonValidator personValidator)
        {
            this.roomRepository = roomRepository;
            this.roomNumberValidator = roomNumberValidator;
            this.stringToRoomParser = stringToRoomParser;
            this.personValidator = personValidator;
        }

        public void ImportRooms(List<string> roomsAsString)
        {
            List<Room> rooms = roomsAsString.Select(this.stringToRoomParser.ParseStringToRoom).ToList();
            this.roomNumberValidator.ValidateUniquenessForRoomNumber(rooms);
            this.personValidator.ValidateUniquenessForPeople(rooms);
            this.roomRepository.Save(rooms);
        }
    }
}
