﻿namespace Neusta.Sitzplan.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using Neusta.Database.Entities;
    using Neusta.Database.Repositories;
    using Neusta.Sitzplan.DTOs;
    using Neusta.Sitzplan.Exceptions;
    using Neusta.Sitzplan.Mappers;

    public class RoomService : IRoomService
    {
        private readonly IRoomMapper roomMapper;

        private readonly IRoomRepository roomRepository;

        public RoomService(IRoomRepository roomRepository, IRoomMapper roomMapper)
        {
            this.roomRepository = roomRepository;
            this.roomMapper = roomMapper;
        }

        public List<RoomDto> GetAllRooms()
        {
            List<Room> rooms = this.roomRepository.GetAllRooms();
            return rooms.Select(this.roomMapper.MapRoomToRoomDto).ToList();
        }

        public RoomDto GetRoom(string roomNumber)
        {
            Room room = this.roomRepository.FindRoom(roomNumber);
            if (room == null)
            {
                string message = $"There is no room with number {roomNumber}";
                throw new RoomNotFoundException(message);
            }

            return this.roomMapper.MapRoomToRoomDto(room);
        }
    }
}
