﻿namespace Neusta.Sitzplan.Validators
{
    using System.Reflection;
    using System.Resources;
    using Microsoft.AspNetCore.Http;
    using Neusta.Sitzplan.Exceptions;

    public class FileValidator : IFileValidator
    {
        private static readonly ResourceManager ResourceManager = new ResourceManager(
            "Neusta.Sitzplan.Resources.ExceptionMessages",
            Assembly.GetExecutingAssembly());

        public void ValidateFile(IFormFile inputFile)
        {
            const string ContentType = "text/csv";

            if (inputFile == null)
            {
                string message = string.Format(
                    ResourceManager.GetString("NoFile") ?? string.Empty,
                    ContentType);
                throw new InvalidCsvDataException(message);
            }

            if (inputFile.Length == 0)
            {
                string message = ResourceManager.GetString("NoDataInFile");
                throw new InvalidCsvDataException(message);
            }
        }
    }
}
