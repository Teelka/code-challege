﻿namespace Neusta.Sitzplan.Validators
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Resources;
    using Neusta.Database.Entities;
    using Neusta.Sitzplan.Exceptions;

    public class PersonValidator : IPersonValidator
    {
        private static readonly ResourceManager ResourceManager = new ResourceManager(
            "Neusta.Sitzplan.Resources.ExceptionMessages",
            Assembly.GetExecutingAssembly());

        public void ValidateUniquenessForPeople(List<Room> rooms)
        {
            List<string> notUniqueLdapuser =
                rooms.Select(room => room.People)
                    .Where(people => people != null)
                    .SelectMany(people => people)
                    .Select(person => person.Ldapuser)
                    .GroupBy(ldapuser => ldapuser)
                    .Where(groupedLdapuser => groupedLdapuser.Count() > 1)
                    .Select(groupedLdapuser => groupedLdapuser.Key)
                    .ToList();

            if (notUniqueLdapuser.Count > 0)
            {
                string notUniqueLdapuserAsString = string.Join(", ", notUniqueLdapuser);

                string message = string.Format(
                    ResourceManager.GetString("NoUniquePeople") ?? string.Empty,
                    notUniqueLdapuserAsString);
                throw new NonUniquePersonException(message);
            }
        }
    }
}
