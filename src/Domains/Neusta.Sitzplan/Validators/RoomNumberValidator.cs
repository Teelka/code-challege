﻿namespace Neusta.Sitzplan.Validators
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Resources;
    using Neusta.Database.Entities;
    using Neusta.Sitzplan.Exceptions;

    public class RoomNumberValidator : IRoomNumberValidator
    {
        private static readonly ResourceManager ResourceManager = new ResourceManager(
            "Neusta.Sitzplan.Resources.ExceptionMessages",
            Assembly.GetExecutingAssembly());

        public void ValidateRoomNumber(string roomNumber)
        {
            const int MaxRoomNumberLength = 4;

            if (roomNumber.Length != MaxRoomNumberLength)
            {
                string message = string.Format(
                    ResourceManager.GetString("MaxRoomNumberLength") ?? string.Empty,
                    MaxRoomNumberLength,
                    roomNumber);
                throw new NonFourDigitRoomNumberException(message);
            }
        }

        public void ValidateUniquenessForRoomNumber(List<Room> rooms)
        {
            List<string> notUniqueRoomNumbers = rooms.Select(room => room.RoomNumber)
                .GroupBy(roomNumber => roomNumber)
                .Where(groupedRoomNumber => groupedRoomNumber.Count() > 1)
                .Select(groupedRoomNumber => groupedRoomNumber.Key)
                .ToList();

            if (notUniqueRoomNumbers.Count > 0)
            {
                string notUniqueRoomNumbersAsString = string.Join(", ", notUniqueRoomNumbers);
                string message = string.Format(
                    ResourceManager.GetString("NoUniqueRoom") ?? string.Empty,
                    notUniqueRoomNumbersAsString);
                throw new NonUniqueRoomException(message);
            }
        }
    }
}
