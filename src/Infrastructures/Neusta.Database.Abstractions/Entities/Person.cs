﻿namespace Neusta.Database.Entities
{
    using System.ComponentModel.DataAnnotations;

    public class Person
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Title { get; set; }

        public string NameAddition { get; set; }

        [Key]
        public string Ldapuser { get; set; }
    }
}
