﻿namespace Neusta.Database.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Room
    {
        [Key]
        public string RoomNumber { get; set; }

        public List<Person> People { get; set; }
    }
}
