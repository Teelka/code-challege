﻿namespace Neusta.Database.Repositories
{
    using System.Collections.Generic;
    using Neusta.Database.Entities;

    public interface IRoomRepository
    {
        List<Room> GetAllRooms();

        Room FindRoom(string roomNumber);

        void Save(List<Room> rooms);
    }
}
