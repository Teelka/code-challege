﻿namespace Neusta.Database.Contexts
{
    using Microsoft.EntityFrameworkCore;
    using Neusta.Database.Entities;

    public class SitzplanDbContext : DbContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<Room> Rooms { get; set; }

        public SitzplanDbContext(DbContextOptions dbContextOptions)
            : base(dbContextOptions)
        {
        }
    }
}
