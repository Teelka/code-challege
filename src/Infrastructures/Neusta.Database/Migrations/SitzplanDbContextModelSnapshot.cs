﻿// <auto-generated />

namespace Neusta.Database.Migrations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Infrastructure;
    using Neusta.Database.Contexts;
    using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

    [DbContext(typeof(SitzplanDbContext))]
    internal class SitzplanDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                .HasAnnotation("ProductVersion", "3.1.8")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity(
                "Neusta.Database.Entities.Person",
                b =>
                {
                    b.Property<string>("Ldapuser")
                        .HasColumnType("text");

                    b.Property<string>("FirstName")
                        .HasColumnType("text");

                    b.Property<string>("LastName")
                        .HasColumnType("text");

                    b.Property<string>("NameAddition")
                        .HasColumnType("text");

                    b.Property<string>("RoomNumber")
                        .HasColumnType("text");

                    b.Property<string>("Title")
                        .HasColumnType("text");

                    b.HasKey("Ldapuser");

                    b.HasIndex("RoomNumber");

                    b.ToTable("People");
                });

            modelBuilder.Entity(
                "Neusta.Database.Entities.Room",
                b =>
                {
                    b.Property<string>("RoomNumber")
                        .HasColumnType("text");

                    b.HasKey("RoomNumber");

                    b.ToTable("Rooms");
                });

            modelBuilder.Entity(
                "Neusta.Database.Entities.Person",
                b =>
                {
                    b.HasOne("Neusta.Database.Entities.Room", null)
                        .WithMany("People")
                        .HasForeignKey("RoomNumber");
                });
#pragma warning restore 612, 618
        }
    }
}
