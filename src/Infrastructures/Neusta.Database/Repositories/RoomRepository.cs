﻿namespace Neusta.Database.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using Neusta.Database.Contexts;
    using Neusta.Database.Entities;

    public class RoomRepository : IRoomRepository
    {
        private readonly SitzplanDbContext sitzplanDbContext;

        public RoomRepository(SitzplanDbContext sitzplanDbContext)
        {
            this.sitzplanDbContext = sitzplanDbContext;
        }

        public List<Room> GetAllRooms()
        {
            return this.sitzplanDbContext.Rooms
                .Include(room => room.People)
                .ToList();
        }

        public Room FindRoom(string number)
        {
            return this.sitzplanDbContext.Rooms
                .Include(room => room.People)
                .FirstOrDefault(room => room.RoomNumber.Equals(number));
        }

        public void Save(List<Room> rooms)
        {
            this.sitzplanDbContext.People.RemoveRange(this.sitzplanDbContext.People);
            this.sitzplanDbContext.Rooms.RemoveRange(this.sitzplanDbContext.Rooms);
            rooms.ForEach(room => this.sitzplanDbContext.Add(room));

            this.sitzplanDbContext.SaveChanges();
        }
    }
}
